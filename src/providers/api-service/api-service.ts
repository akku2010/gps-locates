import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import "rxjs/add/operator/timeout";
import { LoadingController, ToastController } from 'ionic-angular';

@Injectable()
export class ApiServiceProvider {
  private headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });

  googleMapsAPIKey: string = "AIzaSyCmSXnoBBGekErvqBvu5N7dbeqdH1pHp7c";
  mainUrl: string = "https://www.oneqlik.in/";
  usersURL: string;
  devicesURL: string;
  gpsURL: string;
  geofencingURL: string;
  trackRouteURL: string;
  groupURL: string;
  notifsURL: string;
  stoppageURL: string;
  summaryURL: string;
  shareURL: string;
  appId = "OneQlikVTS";
  loading: any;
  loading1: any;
  toast: any;
  constructor(
    public http: Http,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController) {
    console.log('Hello ApiServiceProvider Provider');
    // this.ionViewDidLoad();
    this.callBaseURL();
  }

  // ionViewDidLoad() {
  //   console.log("ApiServiceProvider ionViewDidEnter")
  //   this.callBaseURL();
  //   debugger
  //   if (localStorage.getItem('ENTERED_BASE_URL') !== null) {
  //     console.log("check mail urk: ", localStorage.getItem('ENTERED_BASE_URL'));
  //     this.mainUrl = JSON.parse(localStorage.getItem('ENTERED_BASE_URL')) + '/';
  //     console.log('main url check =: ', this.mainUrl);
  //     this.usersURL = this.mainUrl + "users/";
  //     this.devicesURL = this.mainUrl + "devices";
  //     this.gpsURL = this.mainUrl + "gps";
  //     this.geofencingURL = this.mainUrl + "geofencing";
  //     this.trackRouteURL = this.mainUrl + "trackRoute";
  //     this.groupURL = this.mainUrl + "groups/";
  //     this.notifsURL = this.mainUrl + "notifs";
  //     this.stoppageURL = this.mainUrl + "stoppage";
  //     this.summaryURL = this.mainUrl + "summary";
  //     this.shareURL = this.mainUrl + "share";
  //   } else {
  //     if (localStorage.getItem('BASE_URL') !== null) {
  //       console.log("check mail urk: ", localStorage.getItem('BASE_URL'));
  //       this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
  //       console.log('main url check =: ', this.mainUrl);
  //       this.usersURL = this.mainUrl + "users/";
  //       this.devicesURL = this.mainUrl + "devices";
  //       this.gpsURL = this.mainUrl + "gps";
  //       this.geofencingURL = this.mainUrl + "geofencing";
  //       this.trackRouteURL = this.mainUrl + "trackRoute";
  //       this.groupURL = this.mainUrl + "groups/";
  //       this.notifsURL = this.mainUrl + "notifs";
  //       this.stoppageURL = this.mainUrl + "stoppage";
  //       this.summaryURL = this.mainUrl + "summary";
  //       this.shareURL = this.mainUrl + "share";
  //     }
  //   }
  // }

  callBaseURL() {

    if (localStorage.getItem("ENTERED_BASE_URL") === null) {
      let url = "https://www.oneqlik.in/pullData/getUrlnew";
      this.getSOSReportAPI(url)
        .subscribe((data) => {
          console.log("base url: ", data);
          if (data.url) {
            localStorage.setItem("BASE_URL", JSON.stringify(data.url));
          }
        });
    } else {
      localStorage.setItem("BASE_URL", localStorage.getItem("ENTERED_BASE_URL"));
    }

    if (localStorage.getItem('ENTERED_BASE_URL') !== null) {
      console.log("check mail urk: ", localStorage.getItem('ENTERED_BASE_URL'));
      this.mainUrl = JSON.parse(localStorage.getItem('ENTERED_BASE_URL')) + '/';
      console.log('main url check =: ', this.mainUrl);
      this.usersURL = this.mainUrl + "users/";
      this.devicesURL = this.mainUrl + "devices";
      this.gpsURL = this.mainUrl + "gps";
      this.geofencingURL = this.mainUrl + "geofencing";
      this.trackRouteURL = this.mainUrl + "trackRoute";
      this.groupURL = this.mainUrl + "groups/";
      this.notifsURL = this.mainUrl + "notifs";
      this.stoppageURL = this.mainUrl + "stoppage";
      this.summaryURL = this.mainUrl + "summary";
      this.shareURL = this.mainUrl + "share";
    } else {
      if (localStorage.getItem('BASE_URL') !== null) {
        console.log("check mail urk: ", localStorage.getItem('BASE_URL'));
        this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
        console.log('main url check =: ', this.mainUrl);
        this.usersURL = this.mainUrl + "users/";
        this.devicesURL = this.mainUrl + "devices";
        this.gpsURL = this.mainUrl + "gps";
        this.geofencingURL = this.mainUrl + "geofencing";
        this.trackRouteURL = this.mainUrl + "trackRoute";
        this.groupURL = this.mainUrl + "groups/";
        this.notifsURL = this.mainUrl + "notifs";
        this.stoppageURL = this.mainUrl + "stoppage";
        this.summaryURL = this.mainUrl + "summary";
        this.shareURL = this.mainUrl + "share";
      }
    }
  }

  ////////////////// LOADING SERVICE /////////////////

  toastMsgStarted() {
    this.toast = this.toastCtrl.create({
      message: "Loading/Refreshing contents please wait...",
      position: "bottom",
      duration: 2000
    });
    return this.toast.present();
  }

  toastMsgDismised() {
    return this.toast.dismiss();
  }

  startLoadingnew(key) {
    var str;
    if (key == 1) {
      str = 'unlocking';
    } else {
      str = 'locking';
    }
    return this.loading1 = this.loadingCtrl.create({
      content: "Please wait for some time, as we are " + str + " your vehicle...",
      spinner: "bubbles"
    });
  }
  stopLoadingnw() {
    return this.loading1.dismiss();
  }

  startLoading() {
    return this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      spinner: "bubbles"
    });
  }

  stopLoading() {
    return this.loading.dismiss();
  }

  getCurrency() {
    return this.http.get('./assets/json/currency.json')
      .map(res => res.json())
  }
  getCountryCode() {
    return this.http.get('./assets/json/countryCode.json')
      .map(res => res.json())
  }
  ////////////////// END LOADING SERVICE /////////////

  updatePOIAPI123(pay) {
    return this.http.post(this.mainUrl + "poi/updatePOI", pay, { headers: this.headers })
      .map(res => res.json());
  }

  updatePOIAPI(pay) {
    return this.http.post(this.mainUrl + "vehtra/poi/updatePOI", pay, { headers: this.headers })
      .map(res => res.json());
  }

  addPOIAPI(payload) {
    return this.http.post(this.mainUrl + "poi/addpoi", payload, { headers: this.headers })
      .map(res => res.json());
  }

  deletePOIAPI(_id) {
    return this.http.get(this.mainUrl + "poi/deletePoi?_id=" + _id, { headers: this.headers })
      .map(res => res.json());
  }
  siginupverifyCall(usersignup) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users";
    // }
    return this.http.post(this.usersURL + "/signUpZogo", usersignup, { headers: this.headers })
      .map(res => res.json());
  }

  resendOtp(phnum) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users";
    // }
    return this.http.post(this.usersURL + "/sendOtpZRides", phnum)
      .map(res => res.json());
  }

  getpoireportAPI(_id, pageNo, fromT, toT, poiId, devid) {
    return this.http.get(this.mainUrl + "poi/poiReport?user=" + _id + "&s=" + pageNo + "&l=9&from=" + fromT + "&to=" + toT + "&poi=" + poiId + "&device=" + devid, { headers: this.headers })
      .map(res => res.json());
  }

  getPoisAPI(id) {
    return this.http.get(this.mainUrl + "poi/getPois?user=" + id)
      .map(res => res.json());
  }

  updateDL(updateDL) {
    return this.http.post(this.mainUrl + 'users' + "/zogoUserUpdate", updateDL, { headers: this.headers })
      .map(res => res.json());
  }

  getsingledevice(id) {
    this.callBaseURL();
    // if (this.devicesURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.devicesURL = this.mainUrl + "devices";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.devicesURL = this.mainUrl + "devices";
    // }
    return this.http.get(this.devicesURL + "/getDevicebyId?deviceId=" + id, { headers: this.headers })
      // return this.http.get("http://192.168.1.18:3000/devices/getDevicebyId?deviceId=" + id, { headers: this.headers })
      .map(res => res.json());
  }

  getSOSReportAPI(url) {
    // return this.http.get("http://192.168.1.20:3000/notifs/SOSReport?from_date=" + starttime + '&to_date=' + endtime + '&dev_id=' + sos_id + '&_u=' + _id, { headers: this.headers })
    return this.http.get(url, { headers: this.headers })
      .map(res => res.json());
  }

  saveGoogleAddressAPI(data) {
    let url = this.mainUrl + "googleAddress/addGoogleAddress";
    return this.http.post(url, data, { headers: this.headers })
      .map(res => res.json());
  }

  urlpasseswithdata(url, data) {
    return this.http.post(url, data, { headers: this.headers })
      .map(res => res.json());
  }

  DealerSearchService(_id, pageno, limit, key) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.get(this.usersURL + 'getAllDealerVehicles?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + key, { headers: this.headers })
      .map(res => res.json());
  }

  deleteDealerCall(deletePayload) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.post(this.usersURL + 'deleteUser', deletePayload, { headers: this.headers })
      .map(res => res.json());
  }

  getDealersCall(_id, pageno, limit, searchKey) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // }
    if (searchKey != undefined)
      // return this.http.get(this.usersURL + 'getDealers?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + searchKey, { headers: this.headers })
      return this.http.get(this.usersURL + 'getAllDealerDetailsMobile?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + searchKey, { headers: this.headers })
        .map(res => res.json());
    else
      // return this.http.get(this.usersURL + 'getDealers?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit, { headers: this.headers })
      return this.http.get(this.usersURL + 'getAllDealerDetailsMobile?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit, { headers: this.headers })
        .map(res => res.json());
  }


  callSearchService(baseURL) {
    return this.http.get(baseURL, { headers: this.headers })
      .map(resp => resp.json());
  }

  callResponse(_id) {
    return this.http.get(this.mainUrl + "trackRouteMap/getRideStatusApp?_id=" + _id, { headers: this.headers })
      // return this.http.get("http://192.168.1.13:3000/trackRouteMap/getRideStatusApp?_id=" + _id, { headers: this.headers })
      .map(resp => resp.json());
  }

  serverLevelonoff(data) {
    this.callBaseURL();
    // if (this.devicesURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.devicesURL = this.mainUrl + "devices";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.devicesURL = this.mainUrl + "devices";
    // }
    return this.http.post(this.devicesURL + "/addCommandQueue", data, { headers: this.headers })
      .timeout(5000)
      .map(res => res.json());
  }

  updatePassword(data) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.post(this.usersURL + "updatePassword", data, { headers: this.headers })
      .timeout(5000)
      .map(res => res.json());
  }

  updateprofile(data) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.post(this.usersURL + 'Account_Edit', data, { headers: this.headers })
      .map(res => res.json());
  }

  getGeofenceCall(_id) {
    this.callBaseURL();
    // if (this.geofencingURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.geofencingURL = this.mainUrl + "geofencing";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.geofencingURL = this.mainUrl + "geofencing";
    // }
    return this.http.get(this.geofencingURL + '/getgeofence?uid=' + _id, { headers: this.headers })
      .map(res => res.json())
  }

  get7daysData(a, t) {
    this.callBaseURL();
    // if (this.gpsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.gpsURL = this.mainUrl + "gps";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.gpsURL = this.mainUrl + "gps";
    // }
    return this.http.get(this.gpsURL + '/getDashGraph?imei=' + a + '&t=' + t, { headers: this.headers })
      .map(res => res.json());
  }

  dataRemoveFuncCall(_id, did) {
    this.callBaseURL();
    // if (this.devicesURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.devicesURL = this.mainUrl + "devices";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.devicesURL = this.mainUrl + "devices";
    // }
    return this.http.get(this.devicesURL + '/RemoveShareDevice?did=' + did + '&uid=' + _id, { headers: this.headers })
      .map(res => res.json())
  }

  tripReviewCall(device_id, stime, etime) {
    this.callBaseURL();
    // if (this.gpsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.gpsURL = this.mainUrl + "gps/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.gpsURL = this.mainUrl + "gps/";
    // }
    return this.http.get(this.gpsURL + '?id=' + device_id + '&from=' + stime + '&to=' + etime, { headers: this.headers })
      .map(res => res.json());
  }

  sendTokenCall(payLoad) {
    this.callBaseURL();
    // if (this.shareURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.shareURL = this.mainUrl + "share/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.shareURL = this.mainUrl + "share/";
    // }
    return this.http.post(this.shareURL + "/propagate", payLoad, { headers: this.headers })
      .map(res => res.json());
  }

  shareLivetrackCall(data) {
    this.callBaseURL();
    // if (this.shareURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.shareURL = this.mainUrl + "share/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.shareURL = this.mainUrl + "share/";
    // }
    return this.http.post(this.shareURL, data, { headers: this.headers })
      .map(res => res.json());
  }

  getDriverList(_id) {
    return this.http.get(this.mainUrl + "driver/getDrivers?userid=" + _id, { headers: this.headers })
      .map(res => res.json());
  }

  filterByDateCall(_id, skip: Number, limit: Number, dates) {
    this.callBaseURL();
    // if (this.notifsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.notifsURL = this.mainUrl + "notifs";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.notifsURL = this.mainUrl + "notifs";
    // }
    var from = new Date(dates.fromDate).toISOString();
    var to = new Date(dates.toDate).toISOString();
    return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  filterByType(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getFilteredcall(_id, skip: Number, limit: Number, key) {
    this.callBaseURL();
    // if (this.notifsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.notifsURL = this.mainUrl + "notifs";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.notifsURL = this.mainUrl + "notifs";
    // }
    return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
      .map(res => res.json());
  }

  getDataOnScroll(_id, skip: Number, limit: Number) {
    this.callBaseURL();
    // if (this.notifsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.notifsURL = this.mainUrl + "notifs";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.notifsURL = this.mainUrl + "notifs";
    // }
    // https://www.oneqlik.in/notifs/getNotifByFilters?from_date=2019-06-13T18:30:00.769Z&to_date=2019-06-14T09:40:16.769Z&user=5cde59324e4d600905f4e690&sortOrder=-1
    // return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
    return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
      .map(res => res.json());
  }

  getACReportAPI(fdate, tdate, userid, imei) {
    this.callBaseURL();
    // if (this.notifsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.notifsURL = this.mainUrl + "notifs";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.notifsURL = this.mainUrl + "notifs";
    // }
    return this.http.get(this.notifsURL + '/ACSwitchReport?from_date=' + fdate + '&to_date=' + tdate + '&user=' + userid + '&device=' + imei, { headers: this.headers })
      .map(res => res.json())
  }

  getDetailACReportAPI(fdate, tdate, userid, imei) {
    this.callBaseURL();
    // if (this.notifsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.notifsURL = this.mainUrl + "notifs";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.notifsURL = this.mainUrl + "notifs";
    // }
    return this.http.get(this.notifsURL + "/acReport?from_date=" + fdate + "&to_date=" + tdate + "&_u=" + userid + "&vname=" + imei, { headers: this.headers })
      .map(res => res.json());
  }

  getVehicleListCall(_url) {
    return this.http.get(_url, { headers: this.headers })
      .map(res => res.json());
  }

  trip_detailCall(_id, starttime, endtime, did) {
    this.callBaseURL();
    if (did == undefined) {
      return this.http.get(this.mainUrl + 'user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime, { headers: this.headers })
        .map(res => res.json());
    } else {
      return this.http.get(this.mainUrl + 'user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime + '&device=' + did, { headers: this.headers })
        .map(res => res.json());
    }
  }

  trackRouteDataCall(data) {
    this.callBaseURL();
    // if (this.trackRouteURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.trackRouteURL = this.mainUrl + "trackRoute/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.trackRouteURL = this.mainUrl + "trackRoute/";
    // }
    return this.http.post(this.trackRouteURL, data, { headers: this.headers })
      .map(res => res.json());
  }

  gettrackRouteCall(_id, data) {
    this.callBaseURL();
    // if (this.trackRouteURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.trackRouteURL = this.mainUrl + "trackRoute";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.trackRouteURL = this.mainUrl + "trackRoute";
    // }
    return this.http.post(this.trackRouteURL + '/' + _id, data, { headers: this.headers })
      .map(res => res.json());
  }

  trackRouteCall(_id) {
    this.callBaseURL();
    // if (this.trackRouteURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.trackRouteURL = this.mainUrl + "trackRoute";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.trackRouteURL = this.mainUrl + "trackRoute";
    // }
    return this.http.delete(this.trackRouteURL + '/' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getRoutesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getStoppageApi(starttime, endtime, Ignitiondevice_id, _id) {
    // if (this.stoppageURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.stoppageURL = this.mainUrl + "stoppage";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.stoppageURL = this.mainUrl + "stoppage";
    // }
    this.callBaseURL();
    return this.http.get(this.stoppageURL + "/stoppageReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getIgiApi(starttime, endtime, Ignitiondevice_id, _id) {
    this.callBaseURL();
    // if (this.notifsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.notifsURL = this.mainUrl + "notifs";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.notifsURL = this.mainUrl + "notifs";
    // }
    return this.http.get(this.notifsURL + "/ignitionReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getOverSpeedApi(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getGeogenceReportApi(starttime, endtime, Ignitiondevice_id, _id) {
    this.callBaseURL();
    // if (this.notifsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.notifsURL = this.mainUrl + "notifs";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.notifsURL = this.mainUrl + "notifs";
    // }
    return this.http.get(this.notifsURL + "/GeoFencingReport?from_date=" + starttime + '&to_date=' + endtime + '&geoid=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getFuelApi(starttime, endtime, Ignitiondevice_id, _id) {
    this.callBaseURL();
    // if (this.notifsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.notifsURL = this.mainUrl + "notifs";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.notifsURL = this.mainUrl + "notifs";
    // }
    return this.http.get(this.notifsURL + "/fuelReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getDistanceReportApi(starttime, endtime, _id, Ignitiondevice_id) {
    this.callBaseURL();
    // if (this.summaryURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.summaryURL = this.mainUrl + "summary";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.summaryURL = this.mainUrl + "summary";
    // }
    return this.http.get(this.summaryURL + "/distance?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
      .map(res => res.json());
  }

  getDailyReport(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getAddressApi(data) {
    return this.http.post(this.mainUrl + "gps/getaddress", data, { headers: this.headers })
      .map(res => res.json())
  }

  getDailyReport1(url, payload) {
    return this.http.post(url, payload, { headers: this.headers })
      .map(res => res.json());
  }

  contactusApi(contactdata) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.post(this.usersURL + "contactous", contactdata, { headers: this.headers })
      .map(res => res.json());
  }

  createTicketApi(contactdata) {
    return this.http.post(this.mainUrl + "customer_support/post_inquiry", contactdata, { headers: this.headers })
      .map(res => res.json());
  }

  getAllNotificationCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addgeofenceCall(data) {
    this.callBaseURL();
    // if (this.geofencingURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.geofencingURL = this.mainUrl + "geofencing";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.geofencingURL = this.mainUrl + "geofencing";
    // }
    return this.http.post(this.geofencingURL + '/addgeofence', data, { headers: this.headers })
      .map(res => res);
  }

  getdevicegeofenceCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  geofencestatusCall(_id, status, entering, exiting) {
    this.callBaseURL();
    // if (this.geofencingURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.geofencingURL = this.mainUrl + "geofencing";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.geofencingURL = this.mainUrl + "geofencing";
    // }
    return this.http.get(this.geofencingURL + '/geofencestatus?gid=' + _id + '&status=' + status + '&entering=' + entering + '&exiting=' + exiting, { headers: this.headers })
      .map(res => res.json());
  }

  deleteGeoCall(_id) {
    this.callBaseURL();
    // if (this.geofencingURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.geofencingURL = this.mainUrl + "geofencing";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.geofencingURL = this.mainUrl + "geofencing";
    // }
    return this.http.get(this.geofencingURL + '/deletegeofence?id=' + _id, { headers: this.headers })
      .map(res => res);
  }

  getallgeofenceCall(_id) {
    this.callBaseURL();
    // if (this.geofencingURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.geofencingURL = this.mainUrl + "geofencing";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.geofencingURL = this.mainUrl + "geofencing";
    // }
    return this.http.get(this.geofencingURL + '/getallgeofence?uid=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  user_statusCall(data) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.post(this.usersURL + 'user_status', data, { headers: this.headers })
      .map(res => res);
  }

  editUserDetailsCall(devicedetails) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.post(this.usersURL + 'editUserDetails', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getAllDealerVehiclesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addGroupCall(devicedetails) {
    this.callBaseURL();
    // if (this.groupURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.groupURL = this.mainUrl + "groups/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.groupURL = this.mainUrl + "groups/";
    // }
    return this.http.post(this.groupURL + 'addGroup', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getVehicleTypesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getAllUsersCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getDeviceModelCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  groupsCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addDeviceCall(devicedetails) {
    this.callBaseURL();
    // if (this.devicesURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.devicesURL = this.mainUrl + "devices";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.devicesURL = this.mainUrl + "devices";
    // }
    return this.http.post(this.devicesURL + '/addDevice', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getAddress(cord) {
    return this.http.post(this.mainUrl + "googleAddress/getGoogleAddress", cord, { headers: this.headers })
      .map(res => res.json());
  }

  getCustomersCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  geofenceCall(_id) {
    this.callBaseURL();
    // if (this.geofencingURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.geofencingURL = this.mainUrl + "geofencing";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.geofencingURL = this.mainUrl + "geofencing";
    // }
    return this.http.get(this.geofencingURL + "/getgeofence?uid=" + _id, { headers: this.headers })
      .map(res => res.json());
  }

  forgotPassApi(mobno) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // }
    // else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.get(this.usersURL + "forgotpwd?cred=" + mobno.cred, { headers: this.headers })
      .map(res => res.json());
  }

  forgotPassMobApi(Passwordset) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.get(this.usersURL + "forgotpwd?phone=" + Passwordset.otpMess + "&otp=" + Passwordset.otp + "&newpwd=" + Passwordset.newpwd + "&cred=" + Passwordset.otpMess, { headers: this.headers })
      .map(res => res.json());
  }

  loginApi(userdata) {
    this.callBaseURL()
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.post(this.usersURL + "LoginWithOtp", userdata, { headers: this.headers })
      .map(res => res.json());
  }

  signupApi(usersignupdata) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.post(this.usersURL + "signUp", usersignupdata, { headers: this.headers })
      .map(res => res.json());
  }

  dashboardcall(_baseUrl) {
    return this.http.get(_baseUrl, { headers: this.headers })
      .map(res => res.json());
  }

  stoppedDevices(_id, email, off_ids) {
    this.callBaseURL();
    // if (this.devicesURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.devicesURL = this.mainUrl + "devices";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.devicesURL = this.mainUrl + "devices";
    // }
    return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email + '&dev=' + off_ids, { headers: this.headers })
      .map(res => res.json());
  }

  livedatacall(_id, email) {
    this.callBaseURL();
    // if (this.devicesURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.devicesURL = this.mainUrl + "devices";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.devicesURL = this.mainUrl + "devices";
    // }
    return this.http.get(this.devicesURL + "/getDeviceByUser?id=" + _id + "&email=" + email, { headers: this.headers })
      .map(res => res.json());
  }

  getdevicesApi(_id, email) {
    this.callBaseURL();
    // if (this.devicesURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.devicesURL = this.mainUrl + "devices";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.devicesURL = this.mainUrl + "devices";
    // }
    return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
      .map(res => res.json());
  }
  getdevicesForAllVehiclesApi(link) {
    return this.http.get(link, { headers: this.headers })
      .timeout(500000000)
      .map(res => res.json());
  }

  ignitionoffCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deviceupdateCall(devicedetail) {
    this.callBaseURL();
    // if (this.devicesURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.devicesURL = this.mainUrl + "devices";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.devicesURL = this.mainUrl + "devices";
    // }
    return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
      // return this.http.post("http://192.168.1.18:3000/devices" + "/deviceupdate", devicedetail, { headers: this.headers })
      .map(res => res.json());
  }

  getDistanceSpeedCall(device_id, from, to) {
    this.callBaseURL();
    // if (this.gpsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.gpsURL = this.mainUrl + "gps";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.gpsURL = this.mainUrl + "gps";
    // }
    return this.http.get(this.gpsURL + '/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  stoppage_detail(_id, from, to, device_id) {
    this.callBaseURL();
    // if (this.stoppageURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.stoppageURL = this.mainUrl + "stoppage";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.stoppageURL = this.mainUrl + "stoppage";
    // }
    return this.http.get(this.stoppageURL + '/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
      .map(res => res.json());
  }

  gpsCall(device_id, from, to) {
    this.callBaseURL();
    // if (this.gpsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.gpsURL = this.mainUrl + "gps";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.gpsURL = this.mainUrl + "gps";
    // }
    // return this.http.get(this.gpsURL + '/v2?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
    return this.http.get(this.gpsURL + '?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  getcustToken(id) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.get(this.usersURL + "getCustumerDetail?uid=" + id)
      .map(res => res.json());
  }

  getSummaryReportApi(starttime, endtime, _id, device_id) {
    this.callBaseURL();
    // if (this.summaryURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.summaryURL = this.mainUrl + "summary";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.summaryURL = this.mainUrl + "summary";
    // }
    return this.http.get(this.summaryURL + "/summaryReport?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
      .map(res => res.json());
  }

  getDaywiseReportApi(starttime, endtime, _id, device_id) {
    this.callBaseURL();
    // if (this.summaryURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.summaryURL = this.mainUrl + "summary";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.summaryURL = this.mainUrl + "summary";
    // }
    return this.http.get(this.summaryURL + "/getDayWiseReport?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
      .map(res => res.json());
  }

  getallrouteCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getSpeedReport(_id, time) {
    this.callBaseURL();
    // if (this.gpsURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.gpsURL = this.mainUrl + "gps";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.gpsURL = this.mainUrl + "gps";
    // }
    return this.http.get(this.gpsURL + '/getGpsSpeedReport?imei=' + _id + '&time=' + time, { headers: this.headers })
      .map(res => res.json());
  }

  // deviceupdateInCall(devicedetail) {
  //   return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
  //     .map(res => res.json());
  // }

  deleteDeviceCall(d_id) {
    this.callBaseURL();
    // if (this.devicesURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.devicesURL = this.mainUrl + "devices";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.devicesURL = this.mainUrl + "devices";
    // }
    return this.http.get(this.devicesURL + "/deleteDevice?did=" + d_id, { headers: this.headers })
      // .map(res => res.json());
      .map(res => res);
  }

  deviceShareCall(data) {
    this.callBaseURL();
    // if (this.devicesURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.devicesURL = this.mainUrl + "devices";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.devicesURL = this.mainUrl + "devices";
    // }
    return this.http.get(this.devicesURL + "/deviceShare?email=" + data.email + "&uid=" + data.uid + "&did=" + data.did, { headers: this.headers })
      .map(res => res.json());
  }

  pushnotifyCall(pushdata) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.post(this.usersURL + "PushNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }

  pullnotifyCall(pushdata) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.post(this.usersURL + "PullNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }


  getGroupCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deleteGroupCall(d_id) {
    this.callBaseURL();
    // if (this.groupURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.groupURL = this.mainUrl + "groups/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.groupURL = this.mainUrl + "groups/";
    // }
    return this.http.get(this.groupURL + "deleteGroup?_id=" + d_id, { headers: this.headers })
      .map(res => res.json());
  }
  deleteCustomerCall(data) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.post(this.usersURL + 'deleteUser', data, { headers: this.headers })
      .map(res => res.json());
  }

  getAllDealerCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  route_details(_id, user_id) {
    this.callBaseURL();
    // if (this.trackRouteURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.trackRouteURL = this.mainUrl + "trackRoute/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.trackRouteURL = this.mainUrl + "trackRoute/";
    // }
    return this.http.get(this.trackRouteURL + '/routepath/getRoutePathWithPoi?id=' + _id + '&user=' + user_id, { headers: this.headers })
      .map(res => res.json());
  }

  callcustomerSearchService(uid, pageno, limit, seachKey) {
    this.callBaseURL();
    // if (this.usersURL == undefined) {
    //   this.callBaseURL();
    //   if (localStorage.getItem('BASE_URL') != null) {
    //     this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //     this.usersURL = this.mainUrl + "users/";
    //   }
    // } else {
    //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    //   this.usersURL = this.mainUrl + "users/";
    // }
    return this.http.get(this.usersURL + 'getCust?uid=' + uid + '&pageNo=' + pageno + '&size=' + limit + '&search=' + seachKey)
      .map(res => res.json());
  }

}
