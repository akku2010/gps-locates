import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, ModalController, Platform, ViewController, ToastController, Loading, LoadingController, Navbar } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as io from 'socket.io-client';
import * as _ from "lodash";
import { MarkerCluster, GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, GoogleMapsMapTypeId, LatLngBounds, ILatLng, Polygon, GoogleMapsAnimation, Polyline, CircleOptions, Circle, MarkerClusterOptions } from '@ionic-native/google-maps';
import { TranslateService } from '@ngx-translate/core';
import { DrawerState } from 'ion-bottom-drawer';
import { Subscription } from 'rxjs/Subscription';
import { PoiPage } from '../live-single-device/live-single-device';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import * as moment from 'moment';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
// import { LaunchNavigatorOptions, LaunchNavigator } from '@ionic-native/launch-navigator';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-live',
  templateUrl: 'live.html',
})
export class LivePage implements OnInit, OnDestroy {

  // countFlag: number = 0;

  navigateButtonColor: string = '#196796'; //Default Color
  navColor: string = '#fff';

  policeButtonColor: string = '#196796';
  policeColor: string = '#fff';

  petrolButtonColor: string = "#196796";
  petrolColor: string = '#fff';

  shouldBounce = true;
  dockedHeight = 80;
  distanceTop = 200;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 50;
  transition = '0.85s ease-in-out';

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};

  data: any = {};
  _io: any;
  socketSwitch: any = {};
  socketChnl: any = [];
  socketData: any = {};
  allData: any = {};
  userdetails: any;
  showBtn: boolean;
  SelectVehicle: string;
  selectedVehicle: any;
  titleText: string = 'multiple';
  portstemp: any;
  gpsTracking: any;
  power: any;
  currentFuel: any;
  last_ACC: any;
  today_running: string;
  today_stopped: string;
  timeAtLastStop: string;
  distFromLastStop: any;
  lastStoppedAt: string;
  fuel: any;
  total_odo: any;
  todays_odo: any;
  vehicle_speed: any;
  liveVehicleName: any;
  onClickShow: boolean;
  address: any;
  resToken: string;
  liveDataShare: any;
  isEnabled: boolean = false;
  showMenuBtn: boolean = false;
  latlngCenter: any;
  mapHideTraffic: boolean = false;
  mapData: any = [];
  last_ping_on: any;
  geodata: any = [];
  geoShape: any = [];
  generalPolygon: any;
  locations: any = [];
  acModel: any;
  locationEndAddress: any;
  tempaddress: any;
  mapKey: string;
  shwBckBtn: boolean = false;
  recenterMeLat: any;
  recenterMeLng: any;
  menuActive: boolean;
  deviceDeatils: any = {};
  impkey: any;
  showaddpoibtn: boolean = false;
  isSuperAdmin: boolean;
  isDealer: boolean;
  zoomLevel: number = 18;
  voltage: number;
  temporaryMarker: any;
  temporaryInterval: any;
  circleObj: Circle;
  circleTimer: any;
  tempMarkArray: any = [];
  tempreture: number;
  door: number;
  nearbyPolicesArray: any = [];
  nearbyPetrolArray: any = [];
  socketurl: string = "https://www.oneqlik.in";
  displayNames: boolean = true;
  temporaryMarker123: Marker;
  t_veicle_count: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    public elementRef: ElementRef,
    private socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public plt: Platform,
    private viewCtrl: ViewController,
    private translate: TranslateService,
    private toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider,
    private sqlite: SQLite
    // private loadingCtrl: LoadingController
    // private launchNavigator: LaunchNavigator,
  ) {
    if (localStorage.getItem('Total_Vech') !== null) {
      this.t_veicle_count = JSON.parse(localStorage.getItem('Total_Vech'));
    }
    this.callBaseURL();
    var selectedMapKey;
    if (localStorage.getItem('MAP_KEY') != null) {
      selectedMapKey = localStorage.getItem('MAP_KEY');
      if (selectedMapKey == this.translate.instant('Hybrid')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      } else if (selectedMapKey == this.translate.instant('Normal')) {
        this.mapKey = 'MAP_TYPE_NORMAL';
      } else if (selectedMapKey == this.translate.instant('Terrain')) {
        this.mapKey = 'MAP_TYPE_TERRAIN';
      } else if (selectedMapKey == this.translate.instant('Satellite')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      }
    } else {
      this.mapKey = 'MAP_TYPE_NORMAL';
    }
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> " + JSON.stringify(this.userdetails));
    this.menuActive = false;

    if (localStorage.getItem('DisplayVehicleName') !== null) {
      if (localStorage.getItem('DisplayVehicleName') == 'OFF') {
        this.displayNames = false;
      }
    }
  }

  car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';

  carIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };

  icons = {
    "car": this.carIcon,
    "bike": this.carIcon,
    "truck": this.carIcon,
    "bus": this.carIcon,
    "user": this.carIcon,
    "jcb": this.carIcon,
    "tractor": this.carIcon,
    "ambulance": this.carIcon,
    "auto": this.carIcon
  }

  resumeListener: Subscription = new Subscription();
  ionViewWillEnter() {
    if (this.plt.is('ios')) {
      this.shwBckBtn = true;
      this.viewCtrl.showBackButton(false);
    }

    this.plt.ready().then(() => {
      this.resumeListener = this.plt.resume.subscribe(() => {
        var today, Christmas;
        today = new Date();
        Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
        var diffMs = (today - Christmas); // milliseconds between now & Christmas
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        if (diffMins >= 5) {
          localStorage.removeItem("backgroundModeTime");
          this.alertCtrl.create({
            message: this.translate.instant("Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?"),
            buttons: [
              {
                text: this.translate.instant('YES PROCEED'),
                handler: () => {
                  this.refreshMe();
                }
              },
              {
                text: this.translate.instant('Back'),
                handler: () => {
                  this.navCtrl.setRoot('DashboardPage');
                }
              }
            ]
          }).present();
        }
      })
    })
  }

  ionViewWillLeave() {
    this.plt.ready().then(() => {
      this.resumeListener.unsubscribe();
    })
  }
  hideMe: boolean = false;
  @ViewChild(Navbar) navBar: Navbar;

  ionViewDidEnter() {
    this.navBar.backButtonClick = (ev: UIEvent) => {
      console.log('this will work in Ionic 3 +');
      this.hideMe = true;
      this.navCtrl.pop({
        animate: true, animation: 'transition-ios', direction: 'back'
      });
    }
    if (localStorage.getItem("navigationFrom") === null) {
      this.initiateMap();  // at first we will load the map..
    }

    if (localStorage.getItem("CLICKED_ALL") != null) {
      this.showMenuBtn = true;
    }
    if (localStorage.getItem("SCREEN") != null) {
      if (localStorage.getItem("SCREEN") === 'live') {
        this.showMenuBtn = true;
      }
    }
    // if (this.socketChnl.length > 0) {
    //   this.ngOnDestroy();
    // }

    this.drawerState = DrawerState.Bottom;
    this.onClickShow = false;

    this.showBtn = false;
    this.SelectVehicle = "Select Vehicle";
    this.selectedVehicle = undefined;
    this._io = io.connect(this.socketurl + '/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._io.on('connect', function (data) {
      console.log('Ignition IO Connected', data);
    });
    ////////////////
    this._io.on('connect_failed', function () {
      console.log("Sorry, there seems to be an issue with the connection!");
    });
    this._io.on('error', function () {
      console.log("socket io got error!");
    })
    this._io.on('reconnecting', function () {
      console.log("socket io trying to reconnect!");
    })
    this._io.on('reconnect_failed', function () {
      console.log("reconnecting failed!");
    })
    this._io.on('disconnect', function () {
      console.log('client socket-io disconnect!')
    });
  }

  ngOnInit() { }

  ngOnDestroy() {
    localStorage.removeItem("CLICKED_ALL");

    for (var i = 0; i < this.socketChnl.length; i++)
      this._io.removeAllListeners(this.socketChnl[i]);
    this._io.on('disconnect', () => {
      this._io.open();
    })
  }

  initiateMap() {
    if (this.allData.map) {
      this.allData.map.remove();
      this.allData.map = this.newMap();
    } else {
      this.allData.map = this.newMap();
    }

    /* Check if traffic mode is On/Off */

    if (localStorage.getItem('TrafficMode') !== null) {
      if (localStorage.getItem('TrafficMode') === 'ON') {
        this.mapHideTraffic = true;
        this.allData.map.setTrafficEnabled(true);
      }
    }
    this.callFunction();

  }

  from: string;
  to: string;
  callFunction() {
    this.to = new Date().toISOString();
    var d = new Date();
    var a = d.setHours(0, 0, 0, 0)
    this.from = new Date(a).toISOString();
    // var _baseUrl = this.apiCall.mainUrl + 'gps/getDashboard?email=' + this.userdetails.email + '&from=' + this.from + '&to=' + this.to + '&id=' + this.userdetails._id;
    // if (this.userdetails.isSuperAdmin == true) {
    //   _baseUrl += '&supAdmin=' + this.userdetails._id;
    // } else {
    //   if (this.userdetails.isDealer == true) {
    //     _baseUrl += '&dealer=' + this.userdetails._id;
    //   }
    // }

    // this.apiCall.getAllDealerVehiclesCall(_baseUrl).subscribe(respData => {
    //   debugger
    //   if (!respData.Total_Vech)
    //     return;

    if (Number(this.t_veicle_count) > 500) {
      this.userDevices();
    } else {
      /* After initializing map, we will get data from sqlite database */
      this.getDataFromSQLiteDB();
    }
    // },
    //   err => {
    //     console.log(err);
    //     this.showToastMsg('Something went wrong.. Please try again in some time..!!');
    //     /* After initializing map, we will get data from sqlite database */
    //     this.getDataFromSQLiteDB();
    //   });
  }

  newMap() {
    let mapOptions = {
      controls: {
        compass: false,
        zoom: false,
        myLocation: true,
        myLocationButton: false,
      },
      mapTypeControlOptions: {
        mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
      },
      gestures: {
        rotate: false,
        tilt: false
      },
      mapType: this.mapKey
    }
    let map = GoogleMaps.create('map_canvas', mapOptions);
    if (this.plt.is('android')) {
      map.setPadding(20, 20, 20, 20);
    }
    return map;
  }

  goBack() {
    this.navCtrl.pop({ animate: true, direction: 'forward' });
  }

  onClickMainMenu(item) {
    this.menuActive = !this.menuActive;
  }

  refreshMe() {
    this.ngOnDestroy();
    this.ionViewDidEnter();
    // this.ngOnInit();
  }

  refreshMe1() {
    let that = this;
    this.ngOnDestroy();
    this._io = io.connect(this.socketurl + '/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    var paramData = that.data;
    this.socketInit(paramData);
  }

  callBaseURL() {
    // debugger
    if (localStorage.getItem("ENTERED_BASE_URL") === null) {
      let url = "https://www.oneqlik.in/pullData/getUrlnew";
      this.apiCall.getSOSReportAPI(url)
        .subscribe((data) => {
          console.log("base url: ", data);
          if (data.url) {
            localStorage.setItem("BASE_URL", JSON.stringify(data.url));
          }
          if (data.socket) {
            localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
          }
          this.getSocketUrl();
        });
    }
  }

  getSocketUrl() {
    if (localStorage.getItem('SOCKET_URL') !== null) {
      this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
    }
  }

  userDevices() {
    var baseURLp;
    let that = this;
    // if (that.allData.map != undefined) {
    //   that.allData.map.remove();
    //   that.allData.map = that.newMap();
    // } else {
    //   that.allData.map = that.newMap();
    // }
    //** Check if traffice mode is on or off **//
    // if (localStorage.getItem('TrafficMode') !== null) {
    //   if (localStorage.getItem('TrafficMode') === 'ON') {
    //     this.mapHideTraffic = true;
    //     this.allData.map.setTrafficEnabled(true);
    //   }
    // }
    //** Check if traffice mode is on or off **//

    baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;

    if (this.userdetails.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.userdetails._id;
      this.isSuperAdmin = true;
    } else {
      if (this.userdetails.isDealer == true) {
        baseURLp += '&dealer=' + this.userdetails._id;
        this.isDealer = true;
      }
    }

    // that.apiCall.startLoading().present();
    that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(resp => {
        // that.apiCall.stopLoading();
        that.portstemp = resp.devices;
        // console.log("list of vehicles :", that.portstemp)
        that.mapData = [];
        that.mapData = resp.devices.map(function (d) {
          if (d.last_location !== undefined) {
            return { lat: d.last_location['lat'], lng: d.last_location['long'] };
          }
        });

        var dummyData;

        dummyData = resp.devices.map(function (d) {
          if (d.last_location === undefined)
            return;
          else {
            return {
              "position": {
                "lat": d.last_location['lat'],
                "lng": d.last_location['long']
              },
              "name": d.Device_Name,
              "icon": {
                "url": that.getIconUrl(d),
                "size": {
                  "width": 20,
                  "height": 40
                }
              }
            };
          }
        });

        dummyData = dummyData.filter(function (element) {
          return element !== undefined;
        });

        console.log("dummy data: ", dummyData);
        // console.log("dummy data: ", this.dummyData());

        let bounds = new LatLngBounds(that.mapData);
        that.allData.map.moveCamera({
          target: bounds,
          zoom: 10
        })

        // this.innerFunc(that.portstemp);
        // this.addCluster(this.dummyData());
        if (that.isSuperAdmin || that.isDealer) {
          for (var t = 0; t < dummyData.length; t++) {
            console.log("check position: ", dummyData[t].position);
          }
          // that.addCluster(this.dummyData());
          that.addCluster(dummyData);

        } else {
          for (var i = 0; i < resp.devices.length; i++) {
            if (resp.devices[i].status != "Expired") {
              if (localStorage.getItem("SocketReconnectingStart")) {
                that.weDidntGetPing(resp.devices[i]);
              } else {
                that.socketInit(resp.devices[i]);
              }
            }
          }
          // that.ngOnDestroy();
          // that.portstemp = resp.devices;
        }

      },
        err => {
          // that.apiCall.stopLoading();
          console.log(err);
        });
  }

  addCluster(data) {
    var small, large;
    if (this.plt.is('android')) {
      small = "./assets/markercluster/small.png";
      large = "./assets/markercluster/large.png";
    } else if (this.plt.is('ios')) {
      small = "www/assets/markercluster/small.png";
      large = "www/assets/markercluster/large.png";
    }
    let options: MarkerClusterOptions = {
      markers: data,
      icons: [
        {
          min: 3,
          max: 9,
          url: small,
          size: {
            height: 29,
            width: 29
          },
          label: {
            color: "white"
          }
        },
        {
          min: 10,
          url: large,
          size: {
            height: 37,
            width: 37
          },
          label: {
            color: "white"
          }
        }
      ],
      boundsDraw: false,
      maxZoomLevel: 15
    };
    let markerCluster: MarkerCluster = this.allData.map.addMarkerClusterSync(options);

    markerCluster.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
      let marker: Marker = params[1];
      marker.setTitle(marker.get("name"));
      marker.setSnippet(marker.get("address"));
      marker.showInfoWindow();
    });

  }

  navigateFromCurrentLoc() {
    window.open("https://www.google.com/maps/dir/?api=1&destination=" + this.recenterMeLat + "," + this.recenterMeLng + "&travelmode=driving", "_blank");
  }

  calcRoute(start, end) {
    this.allData.AIR_PORTS = [];
    var directionsService = new google.maps.DirectionsService();
    let that = this;
    var request = {
      origin: start,
      destination: end,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING
    };

    directionsService.route(request, function (response, status) {

      if (status == google.maps.DirectionsStatus.OK) {
        var path = new google.maps.MVCArray();
        for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
          path.push(response.routes[0].overview_path[i]);

          that.allData.AIR_PORTS.push({
            lat: path.g[i].lat(), lng: path.g[i].lng()
          });
          // debugger
          if (that.allData.AIR_PORTS.length > 1) {
            that.allData.map.addPolyline({
              'points': that.allData.AIR_PORTS,
              'color': '#4aa9d5',
              'width': 4,
              'geodesic': true,
            }).then((poly: Polyline) => {
              that.polyLines.push(poly);
              that.getTravelDetails(start, end);

              //   that.showBtn = true;
            })
          }
        }

      }
    });
  }

  _id: any;
  expectation: any = {};
  polyLines: any[] = [];
  marksArray: any[] = [];
  service: any;

  // service = new google.maps.DistanceMatrixService();

  getTravelDetails(source, dest) {
    let that = this;
    that.service = new google.maps.DistanceMatrixService();
    this._id = setInterval(() => {
      if (localStorage.getItem("livepagetravelDetailsObject") != null) {
        if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
          that.expectation = JSON.parse(localStorage.getItem("livepagetravelDetailsObject"));
          console.log("expectation: ", that.expectation)
        } else {
          clearInterval(this._id);
        }
      }
    }, 3000)
    that.service.getDistanceMatrix({
      origins: [source],
      destinations: [dest],
      travelMode: 'DRIVING'
    }, that.callback);
    // that.apiCall.stopLoading();
  }

  callback(response, status) {
    let travelDetailsObject;
    if (status == 'OK') {
      var origins = response.originAddresses;
      for (var i = 0; i < origins.length; i++) {
        var results = response.rows[i].elements;
        for (var j = 0; j < results.length; j++) {
          var element = results[j];
          var distance = element.distance.text;
          var duration = element.duration.text;
          travelDetailsObject = {
            distance: distance,
            duration: duration
          }
        }
      }
      localStorage.setItem("livepagetravelDetailsObject", JSON.stringify(travelDetailsObject));
    }
  }

  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE' || maptype == 'HYBRID') {
      // this.ngOnDestroy();
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
      // this.someFunc();
    } else {
      if (maptype == 'TERRAIN') {
        // this.ngOnDestroy();
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
        // this.someFunc();
      } else {
        if (maptype == 'NORMAL') {
          // this.ngOnDestroy();
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
          // this.someFunc();
        }
      }
    }
  }

  someFunc() {
    var runningDevices = [];
    let that = this;
    that._io = undefined;
    that._io = io.connect(this.socketurl + '/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    that._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    // debugger
    // var paramData = that.data;
    // this.socketInit(paramData);
    if (that.isSuperAdmin || that.isDealer) {

    } else {
      if (that.selectedVehicle === undefined) {
        // setTimeout(() => {
        //   for (var i = 0; i < that.portstemp.length; i++) {
        //     if (that.portstemp[i].status != "Expired") {
        //       // debugger
        //       if (that.portstemp[i].status === "RUNNING") {
        //         if (runningDevices.length <= 5) {
        //           runningDevices.push(that.portstemp[i]);
        //         }
        //       }
        //     }
        //   }
        //   for (var j = 0; j < runningDevices.length; j++) {
        //     console.log("runningDevices[i]: ", runningDevices[j]);
        //     that.socketInit(runningDevices[j]);
        //   }
        // }, 3000);
      } else if (that.selectedVehicle !== undefined) {
        that.socketInit(that.selectedVehicle);
      }

    }
  }

  settings() {
    let that = this;
    let profileModal = this.modalCtrl.create('DeviceSettingsPage', {
      param: that.deviceDeatils
    });
    profileModal.present();

    profileModal.onDidDismiss(() => {
      for (var i = 0; i < that.socketChnl.length; i++)
        that._io.removeAllListeners(that.socketChnl[i]);

      var paramData = this.navParams.get("device");
      this.temp(paramData);
    })
  }

  addPOI() {
    let that = this;
    let modal = this.modalCtrl.create(PoiPage, {
      param1: that.allData[that.impkey]
    });
    modal.onDidDismiss((data) => {
      console.log(data)
      let that = this;
      that.showaddpoibtn = false;
    });
    modal.present();
  }

  onSelectMapOption(type) {
    let that = this;
    if (type == 'locateme') {
      that.allData.map.setCameraTarget(that.latlngCenter);
    } else {
      if (type == 'mapHideTraffic') {
        // this.ngOnDestroy();
        that.mapHideTraffic = !that.mapHideTraffic;
        if (that.mapHideTraffic) {
          that.allData.map.setTrafficEnabled(true);
          // this.someFunc();
        } else {
          that.allData.map.setTrafficEnabled(false);
          // this.someFunc();
        }
      } else {
        if (type == 'showGeofence') {
          console.log("Show Geofence")
          if (that.generalPolygon != undefined) {
            that.generalPolygon.remove();
            that.generalPolygon = undefined;
          } else {
            that.callGeofence();
          }
        }
      }
    }
  }

  callGeofence() {
    this.geoShape = [];
    this.apiCall.startLoading().present();
    this.apiCall.getGeofenceCall(this.userdetails._id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("geofence data=> " + data.length)
        if (data.length > 0) {
          this.geoShape = data.map(function (d) {
            return d.geofence.coordinates[0];
          });
          for (var g = 0; g < this.geoShape.length; g++) {
            for (var v = 0; v < this.geoShape[g].length; v++) {
              this.geoShape[g][v] = this.geoShape[g][v].reverse()
            }
          }

          for (var t = 0; t < this.geoShape.length; t++) {
            this.drawPolygon(this.geoShape[t])
          }
        } else {
          let alert = this.alertCtrl.create({
            message: 'No gofence found..!!',
            buttons: ['OK']
          });
          alert.present();
        }
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  drawPolygon(polyData) {
    let that = this;
    that.geodata = [];
    that.geodata = polyData.map(function (d) {
      return { lat: d[0], lng: d[1] }
    });
    console.log("geodata=> ", that.geodata)

    // let bounds = new LatLngBounds(that.geodata);
    // that.allData.map.moveCamera({
    // target: bounds
    // });
    let GORYOKAKU_POINTS: ILatLng[] = that.geodata;
    console.log("GORYOKAKU_POINTS=> ", GORYOKAKU_POINTS)
    that.allData.map.addPolygon({
      'points': GORYOKAKU_POINTS,
      'strokeColor': '#AA00FF',
      'fillColor': '#00FFAA',
      'strokeWidth': 2
    }).then((polygon: Polygon) => {
      // polygon.on(GoogleMapsEvent.POLYGON_CLICK).subscribe((param) => {
      //   console.log("polygon data=> " + param)
      //   console.log("polygon data=> " + param[1])
      // })
      this.generalPolygon = polygon;
    });
  }

  gotoAll() {
    this.navCtrl.setRoot('LivePage');
    localStorage.setItem("CLICKED_ALL", "CLICKED_ALL");
  }

  shareLive() {
    var data = {
      id: this.liveDataShare._id,
      imei: this.liveDataShare.Device_ID,
      sh: this.userdetails._id,
      ttl: 60 // set to 1 hour by default
    };
    this.apiCall.startLoading().present();
    this.apiCall.shareLivetrackCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.resToken = data.t;
        this.liveShare();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getPOIs() {
    this.allData._poiData = [];
    const _burl = this.apiCall.mainUrl + "poi/getPois?user=" + this.userdetails._id;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_burl)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.allData._poiData = data;

        this.plotPOI();
      });
  }

  plotPOI() {
    let toast = this.toastCtrl.create({
      message: "Plotting POI's please wait...",
      position: 'middle'
    });
    var icurl;
    if (this.plt.is('android')) {
      icurl = './assets/imgs/poiFlag.png';
    } else if (this.plt.is('ios')) {
      icurl = 'www/assets/imgs/poiFlag.png';

    }
    for (var v = 0; v < this.allData._poiData.length; v++) {
      toast.present();
      if (this.allData._poiData[v].poi.location.coordinates !== undefined) {
        this.allData.map.addMarker({
          title: this.allData._poiData[v].poi.poiname,
          position: {
            lat: this.allData._poiData[v].poi.location.coordinates[1],
            lng: this.allData._poiData[v].poi.location.coordinates[0]
          },
          icon: {
            url: icurl,
            size: {
              width: 20,
              height: 20
            }
          },
          animation: GoogleMapsAnimation.BOUNCE
        })
      }
    }
    toast.dismiss();
  }

  liveShare() {
    let that = this;
    var link = this.apiCall.mainUrl + "share/liveShare?t=" + that.resToken;
    that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
  }

  zoomin() {
    let that = this;
    that.allData.map.animateCameraZoomIn();
  }
  zoomout() {
    let that = this;
    that.allData.map.animateCameraZoomOut();
  }

  getIconUrl(data) {
    let that = this;
    var iconUrl;
    if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0) {
      if (that.plt.is('ios')) {
        iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
      } else if (that.plt.is('android')) {
        iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
      }
    } else {
      if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0) {
        if (that.plt.is('ios')) {
          iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
        } else if (that.plt.is('android')) {
          iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
        }
      } else {
        var stricon;
        if (data.status.toLowerCase() === 'out of reach') {
          stricon = "outofreach";
          if (that.plt.is('ios')) {
            iconUrl = 'www/assets/imgs/vehicles/' + stricon + data.iconType + '.png';
          } else if (that.plt.is('android')) {
            iconUrl = './assets/imgs/vehicles/' + stricon + data.iconType + '.png';
          }
        } else {
          if (that.plt.is('ios')) {
            iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
          } else if (that.plt.is('android')) {
            iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
          }
        }
      }
    }
    return iconUrl;
  }

  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    // return h + ':' + m;
    return h + ':' + m + ':' + s;
  }

  otherValues(data) {
    let that = this;
    that.vehicle_speed = data.last_speed;
    that.todays_odo = data.today_odo;
    that.total_odo = data.total_odo;
    if (that.userdetails.fuel_unit == 'LITRE') {
      that.fuel = data.currentFuel;
    } else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
      that.fuel = data.fuel_percent;
    } else {
      that.fuel = data.currentFuel;
    }

    that.voltage = data.battery;

    if (data.last_location) {
      that.getAddress(data.last_location);
    }

    that.last_ping_on = data.last_ping_on;

    var tempvar = new Date(data.lastStoppedAt);
    if (data.lastStoppedAt != null) {
      var fd = tempvar.getTime();
      var td = new Date().getTime();
      var time_difference = td - fd;
      var total_min = time_difference / 60000;
      var hours = total_min / 60
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      that.lastStoppedAt = rhours + ':' + rminutes;
    } else {
      that.lastStoppedAt = '00' + ':' + '00';
    }

    that.distFromLastStop = data.distFromLastStop;
    if (!isNaN(data.timeAtLastStop)) {
      that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
    } else {
      that.timeAtLastStop = '00:00:00';
    }

    that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
    that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
    that.last_ACC = data.last_ACC;
    that.acModel = data.ac;
    that.currentFuel = data.currentFuel;
    that.power = data.power;
    that.gpsTracking = data.gpsTracking;
    that.tempreture = data.temp;
    that.door = data.door;
    that.recenterMeLat = data.last_location.lat;
    that.recenterMeLng = data.last_location.long;
  }

  weDidntGetPing(data) {
    let that = this;
    if (data._id != undefined && data.last_location != undefined) {
      that.otherValues(data);
      if (data.last_location != undefined) {

        that.allData.map.addMarker({
          title: data.Device_Name,
          position: { lat: data.last_location.lat, lng: data.last_location.long },
          icon: {
            url: that.getIconUrl(data),
            size: {
              width: 20,
              height: 40
            }
          },
        }).then((marker: Marker) => {
          if (that.selectedVehicle) {
            that.temporaryMarker = marker;
          } else {
            that.tempMarkArray.push(marker);
          }
          // that.allData[key].mark = marker;
          marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
            .subscribe(e => {
              that.liveVehicleName = data.Device_Name;
              // that.drawerHidden = false;
              that.showaddpoibtn = true;
              that.drawerState = DrawerState.Docked;
              that.onClickShow = true;

            });
          that.getAddress(data.last_location);

        });
      }

    }
  }

  socketInit(pdata, center = false) {
    let that = this;

    that._io.emit('acc', pdata.Device_ID);
    that.socketChnl.push(pdata.Device_ID + 'acc');
    that._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {

      if (d4 != undefined)

        // console.log('d4 data: ', d4);
        (function (data) {

          if (data == undefined) {
            return;
          }

          if (that.selectedVehicle !== undefined) {
            that.reCenterMe();
            var temp123;
            if (that.temporaryMarker) {
              that.temporaryMarker.remove();
              temp123 = "cleared";
            }
            that.temporaryInterval = setInterval(() => {
              if (that.temporaryMarker) {
                that.temporaryMarker.remove();
                clearInterval(that.temporaryInterval);
                console.log("temporaryInterval cleared!!")
              } else {
                if (temp123 === 'cleared') {
                  if (that.temporaryInterval) {
                    clearInterval(that.temporaryInterval);
                  }
                }
              }
            }, 10);
          } else {
            // var temp122;
            if (that.tempMarkArray.length > 0) {
              for (var t = 0; t < that.tempMarkArray.length; t++) {
                that.tempMarkArray[t].remove();
              }
            }
          }
          console.log("on ping: ", data)
          if (data._id != undefined && data.last_location != undefined) {
            var key = data._id;
            that.impkey = data._id;
            that.deviceDeatils = data;
            let ic = _.cloneDeep(that.icons[data.iconType]);
            if (!ic) {
              return;
            }
            ic.path = null;
            if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0)) {
              if (that.plt.is('ios')) {
                ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
              } else if (that.plt.is('android')) {
                ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
              }
            } else {
              if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0)) {
                if (that.plt.is('ios')) {
                  ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                } else if (that.plt.is('android')) {
                  ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
              } else {
                var stricon;
                if (data.status.toLowerCase() === 'out of reach') {
                  stricon = "outofreach";
                  if (that.plt.is('ios')) {
                    ic.url = 'www/assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                  } else if (that.plt.is('android')) {
                    ic.url = './assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                  }
                } else {
                  if (that.plt.is('ios')) {
                    ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                  } else if (that.plt.is('android')) {
                    ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                  }
                }
              }
            }
            that.otherValues(data);
            console.log("icon url: ", ic.url);
            if (data.last_location != undefined) {
              that.getAddress(data.last_location);
            }
            var strStr;
            if (that.allData[key]) {
              strStr = ' Address: ' + that.address + '\n Last Updated: ' + moment(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
              that.socketSwitch[key] = data;
              if (that.allData[key].mark != undefined && that.allData[key].mark1 != undefined) {
                // that.allData[key].mark.setTitle();

                that.allData[key].mark.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
                  // alert('clicked');
                  setTimeout(() => {
                    that.allData[key].mark.hideInfoWindow();
                  }, 2000)
                });
                that.allData[key].mark.setSnippet(strStr);
                that.allData[key].mark.setIcon(ic.url);

                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                var temp = _.cloneDeep(that.allData[key].poly[1]);
                that.allData[key].poly[0] = _.cloneDeep(temp);
                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                if (that.displayNames) {
                  that.allData[key].mark1.setPosition(that.allData[key].poly[1]);
                  that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);

                } else {
                  that.liveTrack(that.allData.map, that.allData[key].mark, null, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);

                }
              } else {
                return;
              }
            }
            else {
              that.allData[key] = {};
              that.socketSwitch[key] = data;
              that.allData[key].poly = [];

              that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });

              if (data.last_location != undefined) {
                strStr = ' Address: ' + that.address + '\n Last Updated: ' + moment(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
                debugger
                if (that.displayNames) {
                  that.showMergedMarkers(data, key, ic, center);
                } else {
                  that.showSimplemarkers(data, key, ic, center);
                }

                // that.allData.map.addMarker({
                //   // title: data.Device_Name,
                //   position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                //   icon: {
                //     url: ic.url,
                //     size: {
                //       height: 40,
                //       width: 20
                //     }
                //   },
                // }).then((marker: Marker) => {
                //   that.allData[key].mark = marker;
                //   marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                //     .subscribe(e => {
                //       if (that.selectedVehicle == undefined) {
                //         that.getAddressTitle(marker);
                //       } else {
                //         that.liveVehicleName = data.Device_Name;
                //         // that.drawerHidden = false;
                //         that.showaddpoibtn = true;
                //         that.drawerState = DrawerState.Docked;
                //         that.onClickShow = true;
                //       }
                //     });
                //   // that.getAddress(data.last_location);
                //   var speed = data.status == "RUNNING" ? data.last_speed : 0;
                //   that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                // });
                // }

                // that.allData.map.addMarker({
                //   title: data.Device_Name,
                //   snippet: strStr,
                //   position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                //   icon: {
                //     url: ic.url,
                //     size: {
                //       width: 20,
                //       height: 40
                //     }
                //   },
                // }).then((marker: Marker) => {

                //   that.allData[key].mark = marker;

                //   // if (that.selectedVehicle == undefined) {
                //   //   marker.showInfoWindow();
                //   // }

                //   // if (that.selectedVehicle != undefined) {
                //   marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                //     .subscribe(e => {
                //       if (that.selectedVehicle == undefined) {
                //         that.getAddressTitle(marker);
                //       } else {
                //         that.liveVehicleName = data.Device_Name;
                //         // that.drawerHidden = false;
                //         that.showaddpoibtn = true;
                //         that.drawerState = DrawerState.Docked;
                //         that.onClickShow = true;
                //       }
                //     });
                //   // that.getAddress(data.last_location);
                //   var speed = data.status == "RUNNING" ? data.last_speed : 0;
                //   that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                // });
                ///////////////
                if (that.selectedVehicle) {
                  // that.addCircleWithAnimation(that.allData[data._id].poly[0].lat, that.allData[data._id].poly[0].lng);
                }
                //////////////
              }
            }

          }
        })(d4)
    })
  }

  liveTrack(map, mark, mark1, coords, speed, delay, center, id, that) {
    // liveTrack(map, mark, mark1, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {

    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);

    if (center) {
      map.setCameraTarget(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;


      // console.log("issue coming from livetrack 1", mark)
      var lat = 0;
      var lng = 0;
      if (mark.getPosition().lat !== undefined || mark.getPosition().lng !== undefined) {
        lat = mark.getPosition().lat;
        lng = mark.getPosition().lng;
      }


      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
        // mark.setIcon(icons);
      }
      function _moveMarker() {

        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          // if (that.selectedVehicle != undefined) {
          //   map.setCameraTarget(new LatLng(lat, lng));
          // }
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          if (that.displayNames) {
            mark1.setPosition(new LatLng(lat, lng));
          }

          /////////
          // that.circleObj.setCenter(new LatLng(lat, lng))
          ////////
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          // if (that.selectedVehicle != undefined) {
          //   map.setCameraTarget(dest);
          // }
          that.latlngCenter = dest;
          mark.setPosition(dest);
          if (that.displayNames) {
            mark1.setPosition(dest);
          }

          /////////
          // that.circleObj.setCenter(new LatLng(dest.lat, dest.lng))
          ////////
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  showSimplemarkers(data, key, ic, center) {
    let that = this;
    that.allData.map.addMarker({
      title: data.Device_Name,
      position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
      icon: {
        url: ic,
        size: {
          height: 40,
          width: 20
        }
      },
    }).then((marker: Marker) => {
      that.allData[key].mark = marker;
      marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
        .subscribe(e => {
          if (that.selectedVehicle == undefined) {
            that.getAddressTitle(marker);
          } else {
            that.liveVehicleName = data.Device_Name;
            // that.drawerHidden = false;
            that.showaddpoibtn = true;
            that.drawerState = DrawerState.Docked;
            that.onClickShow = true;
          }
        });
      // that.getAddress(data.last_location);
      var speed = data.status == "RUNNING" ? data.last_speed : 0;
      that.liveTrack(that.allData.map, that.allData[key].mark, null, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
    });
  }

  showMergedMarkers(data, key, ic, center) {
    let that = this;
    /////////////// merge images using canvas

    let c: any = document.createElement("canvas");
    c.width = 100;
    c.height = 60;
    var ctx = c.getContext("2d");

    ctx.fillStyle = "#1c2f66";
    ctx.fillRect(0, 0, 100, 20);
    // ctx.strokeRect(0, 0, 100, 70);

    ctx.font = "9pt sans-serif";
    ctx.fillStyle = "white";
    ctx.fillText(data.Device_Name, 12, 15);

    var img = c.toDataURL();

    that.allData.map.addMarker({
      // title: data.Device_Name,
      position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
      icon: {
        url: img,
        size: {
          height: 60,
          width: 100
        }
      },
    }).then((marker1: Marker) => {
      that.allData[key].mark1 = marker1;
      that.allData.map.addMarker({
        // title: data.Device_Name,
        position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
        icon: {
          url: ic.url,
          size: {
            height: 40,
            width: 20
          }
        },
      }).then((marker: Marker) => {
        that.allData[key].mark = marker;
        marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
          .subscribe(e => {
            if (that.selectedVehicle == undefined) {
              that.getAddressTitle(marker);
            } else {
              that.liveVehicleName = data.Device_Name;
              // that.drawerHidden = false;
              that.showaddpoibtn = true;
              that.drawerState = DrawerState.Docked;
              that.onClickShow = true;
            }
          });
        // that.getAddress(data.last_location);
        var speed = data.status == "RUNNING" ? data.last_speed : 0;
        that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
      });
    });
  }

  addCircleWithAnimation(lat, lng) {
    let that = this;
    var _radius = 500;
    var rMin = _radius * 1 / 9;
    var rMax = _radius;
    var direction = 1;
    let GOOGLE: ILatLng = {
      "lat": lat,
      "lng": lng
    };

    let circleOption: CircleOptions = {
      'center': GOOGLE,
      'radius': 500,
      'fillColor': 'rgb(216, 6, 34)',
      'fillOpacity': 0.6,
      'strokeColor': '#950417',
      'strokeOpacity': 1,
      'strokeWidth': 0.5
    };
    // Add circle

    let circle: Circle = that.allData.map.addCircleSync(circleOption);
    that.circleObj = circle;

    that.circleTimer = setInterval(() => {
      var radius = circle.getRadius();

      if ((radius > rMax) || (radius < rMin)) {
        direction *= -1;
      }
      var _par = (radius / _radius) - 0.1;
      var opacity = 0.4 * _par;
      var colorString = that.RGBAToHexA(216, 6, 34, opacity);
      circle.setFillColor(colorString);
      circle.setRadius(radius + direction * 10);
    }, 30);
  }

  RGBAToHexA(r, g, b, a) {
    r = r.toString(16);
    g = g.toString(16);
    b = b.toString(16);
    a = Math.round(a * 255).toString(16);

    if (r.length == 1)
      r = "0" + r;
    if (g.length == 1)
      g = "0" + g;
    if (b.length == 1)
      b = "0" + b;
    if (a.length == 1)
      a = "0" + a;

    return "#" + r + g + b + a;
  }

  getAddress(coordinates) {
    let that = this;
    if (!coordinates) {
      that.address = 'N/A';
      return;
    }
    this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        // that.saveAddressToServer(str, coordinates.lat, coordinates.long);
        that.address = str;
      })
  }

  showNearby(key) {
    // for police stations
    var url, icurl;
    if (key === 'police') {
      if (this.policeButtonColor == '#196796') {
        this.policeButtonColor = '#f4f4f4';
        this.policeColor = '#196796';
      } else {
        if (this.policeButtonColor == '#f4f4f4') {
          this.policeButtonColor = '#196796';
          this.policeColor = '#fff';
        }
      }
      if (this.plt.is('android')) {
        icurl = './assets/imgs/police-station.png';
      } else if (this.plt.is('ios')) {
        icurl = 'www/assets/imgs/police-station.png';
      }
      url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=police&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";

    } else if (key === 'petrol') {
      if (this.petrolButtonColor == '#196796') {
        this.petrolButtonColor = '#f4f4f4';
        this.petrolColor = '#196796';
      } else {
        if (this.petrolButtonColor == '#f4f4f4') {
          this.petrolButtonColor = '#196796';
          this.petrolColor = '#fff';
        }
      }
      if (this.plt.is('android')) {
        icurl = './assets/imgs/gas-pump.png';
      } else if (this.plt.is('ios')) {
        icurl = 'www/assets/imgs/gas-pump.png';
      }
      url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=gas_station&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";

    }

    console.log("google maps activity: ", url);
    this.apiCall.getSOSReportAPI(url).subscribe(resp => {
      console.log(resp.status);
      if (resp.status === 'OK') {
        console.log(resp.results);
        console.log(resp);
        var mapData = resp.results.map(function (d) {
          return { lat: d.geometry.location.lat, lng: d.geometry.location.lng };
        })

        let bounds = new LatLngBounds(mapData);

        for (var t = 0; t < resp.results.length; t++) {
          this.allData.map.addMarker({
            title: resp.results[t].name,
            position: {
              lat: resp.results[t].geometry.location.lat,
              lng: resp.results[t].geometry.location.lng
            },
            icon: {
              url: icurl,
              size: {
                height: 35,
                width: 35
              }
            }
          }).then((mark: Marker) => {

          })
        }
        this.allData.map.moveCamera({
          target: bounds
        });
        this.allData.map.setPadding(20, 20, 20, 20);
        this.zoomLevel = 10;
      } else if (resp.status === 'ZERO_RESULTS') {
        if (key === 'police') {
          this.showToastMsg('No nearby police stations found.');
        } else if (key === 'petrol') {
          this.showToastMsg('No nearby petrol pump found.');
        }
      } else if (resp.status === 'OVER_QUERY_LIMIT') {
        this.showToastMsg('Query limit exeed. Please try after some time.');
      } else if (resp.status === 'REQUEST_DENIED') {
        this.showToastMsg('Please check your API key.');
      } else if (resp.status === 'INVALID_REQUEST') {
        this.showToastMsg('Invalid request. Please try again.');
      } else if (resp.status === 'UNKNOWN_ERROR') {
        this.showToastMsg('An unknown error occured. Please try again.');
      }
    })
  }

  showToastMsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    }).present();
  }

  reCenterMe() {
    this.allData.map.animateCamera({
      target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
      // zoom: this.allData.map.getCameraZoom(),
      tilt: 30,
      // bearing: this.allData.map.getCameraBearing(),
      duration: 1800
    }).then(() => {

    })
    // // console.log("getzoom level: " + this.allData.map.getCameraZoom());
    // this.allData.map.moveCamera({
    //   target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
    //   zoom: this.allData.map.getCameraZoom()
    // }).then(() => {

    // })
  }

  getAddressTitle(marker) {
    // if(marker)
    console.log("issue coming from getAddressTitle")
    var payload = {
      "lat": marker.getPosition().lat,
      "long": marker.getPosition().lng,
      "api_id": "1"
    }
    var addr;
    this.apiCall.getAddressApi(payload)
      .subscribe((data) => {
        console.log("got address: " + data.results)
        if (data.results[2] != undefined || data.results[2] != null) {
          addr = data.results[2].formatted_address;
        } else {
          addr = 'N/A';
        }
        marker.setSnippet(addr);
      })
  }

  info(mark, cb) {
    mark.addListener('click', cb);
  }

  setDocHeight() {
    let that = this;
    that.distanceTop = 200;
    that.drawerState = DrawerState.Top;
  }

  temp(data) {
    // debugger
    localStorage.setItem("navigationFrom", "MultiLiveScreen")
    console.log("check selected vehicle: ", this.selectedVehicle)
    console.log("check temp function data: ", data);

    if (data.status === 'Expired') {
      let profileModal = this.modalCtrl.create('ExpiredPage', {
        param: data
      });
      profileModal.present();

      profileModal.onDidDismiss(() => {
        this.selectedVehicle = undefined;
      });

    } else {
      this.navCtrl.push('LiveSingleDevice', { device: data }, {
        animate: true, animation: 'transition-ios'
      });
    }
    // this.navCtrl.push('LiveSingleDevice', { device: data }, {
    //   animate: true, animation: 'transition-ios'
    // });
    // this.titleText = 'single';
    // if (data.status == 'Expired') {
    //   let profileModal = this.modalCtrl.create('ExpiredPage', {
    //     param: data
    //   });
    //   profileModal.present();

    //   profileModal.onDidDismiss(() => {
    //     this.selectedVehicle = undefined;
    //   });

    // } else {
    //   let that = this;

    //   that.data = data;
    //   that.liveDataShare = data;
    //   // that.drawerHidden = true;
    //   that.drawerState = DrawerState.Bottom;
    //   that.onClickShow = false;

    //   if (that.allData.map != undefined) {
    //     // that.allData.map.clear();
    //     that.allData.map.remove();
    //   }

    //   // console.log("on select change data=> " + JSON.stringify(data));
    //   debugger
    //   for (var i = 0; i < that.socketChnl.length; i++)
    //     that._io.removeAllListeners(that.socketChnl[i]);
    //   that.allData = {};
    //   that.socketChnl = [];
    //   that.socketSwitch = {};
    //   if (data) {
    //     if (data.last_location) {

    //       let mapOptions = {
    //         // backgroundColor: 'white',
    //         controls: {
    //           compass: false,
    //           zoom: false,
    //           mapToolbar: true,
    //           myLocation: true,
    //           myLocationButton: false,
    //         },
    //         gestures: {
    //           rotate: false,
    //           tilt: false
    //         },
    //         mapType: that.mapKey
    //       }
    //       let map = GoogleMaps.create('map_canvas', mapOptions);
    //       map.animateCamera({
    //         target: { lat: 20.5937, lng: 78.9629 },
    //         zoom: 15,
    //         duration: 1000,
    //         padding: 0  // default = 20px
    //       });

    //       map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
    //       // map.moveCamera({
    //       //   target: {
    //       //     lat: data.last_location['lat'], lng: data.last_location['long']
    //       //   }
    //       // });
    //       map.setPadding(20, 20, 20, 20);
    //       // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
    //       that.allData.map = map;
    //       // that.weDidntGetPing(data);

    //       // that.socketInit(data);
    //       // that.socketInitForSingleVehicle(data);
    //       that.weDidntGetPingForSingleVehicle(data);
    //     } else {
    //       that.allData.map = that.newMap();
    //       // that.socketInit(data);
    //       that.socketInitForSingleVehicle(data);
    //     }

    //     if (that.selectedVehicle != undefined) {
    //       // that.drawerHidden = false;
    //       that.drawerState = DrawerState.Docked;
    //       that.onClickShow = true;
    //     }
    //   }
    //   that.showBtn = true;
    // }
  }
  socketInitForSingleVehicle(pdata, center = false) {

    this._io.emit('acc', pdata.Device_ID);
    this.socketChnl.push(pdata.Device_ID + 'acc');
    let that = this;


    this._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
      if (d4 != undefined)
        console.log("ping data: => ", d4);
      (function (data) {

        if (data == undefined) {
          return;
        }
        localStorage.setItem("gotPing", "true");
        that.reCenterMe();

        localStorage.setItem("pdata", JSON.stringify(data))

        if (data._id != undefined && data.last_location != undefined) {
          var key = data._id;
          that.impkey = data._id;
          that.deviceDeatils = data;

          that.otherValues(data);
          var strStr;
          // debugger
          if (that.allData[key]) {
            strStr = ' Address: ' + that.address + '\n Last Updated: ' + moment(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
            that.socketSwitch[key] = data;
            console.log("check mark is avalible or not? ", that.allData[key].mark)
            if (that.allData[key].mark !== undefined) {
              that.allData[key].mark.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
                // alert('clicked');
                setTimeout(() => {
                  that.allData[key].mark.hideInfoWindow();
                }, 2000)
              });
              that.allData[key].mark.setSnippet(strStr);
              that.allData[key].mark.setIcon(that.getIconUrl(data));
              that.allData[key].mark.setPosition(that.allData[key].poly[1]);

              var temp = _.cloneDeep(that.allData[key].poly[1]);
              that.allData[key].poly[0] = _.cloneDeep(temp);
              that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

              var speed = data.status == "RUNNING" ? data.last_speed : 0;
              that.getAddress(data.last_location);
              that.liveTrackForSingleVehicle(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);

            } else {
              return;
            }
          }
          else {
            that.allData[key] = {};
            that.socketSwitch[key] = data;
            that.allData[key].poly = [];
            if (data.sec_last_location.lat !== null && data.sec_last_location.long !== null) {
              that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
            } else {
              that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
            }
            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
            if (data.last_location != undefined) {
              console.log("we got pung: ", that.getIconUrl(data));
              // let position: ILatLng = {
              //   lat: that.allData[key].poly[0].lat,
              //   lng: that.allData[key].poly[0].lng
              // };
              // debugger
              if (that.temporaryMarker123 !== undefined) {
                that.temporaryMarker123.setTitle(data.Device_Name);
                // that.temporaryMarker.setIcon(that.getIconUrl(data));
                that.temporaryMarker123.setPosition(that.allData[key].poly[0]);

                that.allData[key].mark = that.temporaryMarker123;

                // that.temporaryMarker.remove();
                that.allData[key].mark.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                  .subscribe(e => {
                    that.liveVehicleName = data.Device_Name;
                    // that.showActionSheet = true;
                    that.showaddpoibtn = true;

                    that.getAddressTitle(that.allData[key].mark);

                  });
                that.getAddress(data.last_location);
                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                that.liveTrackForSingleVehicle(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
              } else {
                that.allData.map.addMarker({
                  title: data.Device_Name,
                  position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                  icon: {
                    url: that.getIconUrl(data),
                    size: {
                      height: 40,
                      width: 20
                    }
                  },
                }).then((marker: Marker) => {

                  that.allData[key].mark = marker;
                  marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                    .subscribe(e => {
                      that.liveVehicleName = data.Device_Name;
                      // that.showActionSheet = true;
                      that.showaddpoibtn = true;

                      that.getAddressTitle(marker);

                    });
                  that.getAddress(data.last_location);
                  var speed = data.status == "RUNNING" ? data.last_speed : 0;
                  that.liveTrackForSingleVehicle(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                });
              }


              ///////////////
              // that.addCircleWithAnimation(that.allData[data._id].poly[0].lat, that.allData[data._id].poly[0].lng);
              //////////////
            }
          }
        }
      })(d4)
    })
  }

  liveTrackForSingleVehicle(map, mark, coords, speed, delay, center, id, that) {
    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);
    // if (center) {
    //   map.setCameraTarget(coords[0]);
    // }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
      }
      function _moveMarker() {
        // debugger
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {

          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.mapTrail) {
            that.showTrail(lat, lng, map);
          } else {
            console.log(that.allData.polylineID)
            // console.log(that.allData.polylineID.length)

            if (that.allData.polylineID) {
              that.tempArray = [];
              that.allData.polylineID.remove();
            }
          }

          // map.moveCamera({
          //   target: new LatLng(lat, lng)
          // });
          // map.setCameraTarget(new LatLng(lat, lng));
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          /////////
          // that.circleObj.setCenter(new LatLng(lat, lng))
          ////////
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
            // map.moveCamera({
            //   target: {
            //     lat: dest.lat,
            //     lng: dest.lng
            //   },
            //   tilt: 30,
            //   bearing: head
            // });
          }
          if (that.mapTrail) {
            that.showTrail(dest.lat, dest.lng, map);
          }
          else {
            if (that.allData.polylineID) {
              that.tempArray = [];
              that.allData.polylineID.remove();
            }
          }
          // map.moveCamera({
          //   target: {
          //     lat: dest.lat,
          //     lng: dest.lng
          //   }
          // });
          // map.setCameraTarget(dest);
          that.latlngCenter = dest;
          mark.setPosition(dest);
          /////////
          // that.circleObj.setCenter(new LatLng(dest.lat, dest.lng))
          ////////
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }
  weDidntGetPingForSingleVehicle(data) {
    console.log("I came here in weDidntGetPing function")
    let that = this;
    if (data._id != undefined && data.last_location != undefined) {
      that.otherValues(data);
      if (data.last_location != undefined) {
        console.log("we did not get pung: ", that.getIconUrl(data))
        debugger
        // if (localStorage.getItem('gotPing') === null) {
        that.allData.map.addMarker({
          title: data.Device_Name,
          position: { lat: data.last_location.lat, lng: data.last_location.long },
          icon: {
            url: that.getIconUrl(data),
            size: {
              height: 40,
              width: 20
            }
          },
        }).then((marker: Marker) => {
          // debugger
          that.temporaryMarker123 = marker;
          // that.allData[key].mark = marker;
          marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
            .subscribe(e => {
              that.liveVehicleName = data.Device_Name;
              // that.showActionSheet = true;
              that.showaddpoibtn = true;

              that.getAddressTitle(marker);

            });
          that.getAddress(data.last_location);
          that.socketInitForSingleVehicle(data);

        });
        // }
      }
    }
  }

  ///////////// CORDOVA SQLite CODE ////////////////////////////
  // db: SQLiteObject;
  getDataFromSQLiteDB() {
    let that = this;
    this.sqlite.create({
      name: 'oneqlik_vts.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql(`
      CREATE TABLE IF NOT EXISTS vehicle_list(
        _id VARCHAR,
        Device_Name TEXT,
        Device_ID BIGINT,
        supAdmin VARCHAR,
        Dealer VARCHAR,
        expiration_date VARCHAR,
        status_updated_at VARCHAR,
        fuel_percent FLOAT,
        currentFuel FLOAT,
        last_speed INT,
        created_on VARCHAR,
        today_odo FLOAT,
        contact_number BIGINT,
        iconType TEXT,
        vehicleType TEXT,
        status TEXT,
        last_lat FLOAT,
        last_lng FLOAT,
        total_odo FLOAT,
        battery INT,
        last_ping_on VARCHAR,
        lastStoppedAt VARCHAR,
        distFromLastStop FLOAT,
        timeAtLastStop FLOAT,
        today_stopped FLOAT,
        today_running FLOAT,
        last_ACC VARCHAR,
        ac INT,
        power VARCHAR,
        gpsTracking VARCHAR,
        temp VARCHAR,
        door VARCHAR,
        ignitionLock VARCHAR,
        towAlert VARCHAR,
        theftAlert VARCHAR
        )`, [])
        .then(res => {
          console.log(res);
          db.executeSql('SELECT * FROM vehicle_list', [])
            .then(res => {
              debugger
              if (res.rows.length > 0) {
                var temparray = [];
                for (var i = 0; i < res.rows.length; i++) {
                  temparray.push(res.rows.item(i))
                }
                that.mapData = [];
                that.mapData = temparray.map(function (d) {
                  if (d.last_lat !== undefined && d.last_lng !== undefined) {
                    // return { lat: d.last_lat, lng: d.last_lng };
                    if (d.last_lat !== 0 && d.last_lng !== 0) {
                      return { lat: d.last_lat, lng: d.last_lng };
                    }
                  }
                });
                let bounds = new LatLngBounds(that.mapData);
                that.allData.map.moveCamera({
                  target: bounds,
                  zoom: 8
                });
                that.doFurtherLogic(temparray);
              } else {
                that.userDevices();
              }
            })
            .catch(e => console.log(e));

        })
        .catch(e => {
          console.log(e)
        });
    });
  }

  doFurtherLogic(array) {
    let that = this;
    that.portstemp = array;
    if (that.isSuperAdmin || that.isDealer) {
      var dummyData;
      dummyData = array.map(function (d) {
        if (d.last_lat === undefined && d.last_lng === undefined)
          return;
        else {
          return {
            "position": {
              "lat": d.last_lat,
              "lng": d.last_lng
            },
            "name": d.Device_Name,
            "icon": {
              "url": that.getIconUrl(d),
              "size": {
                "width": 20,
                "height": 40
              }
            }
          };
        }
      });

      dummyData = dummyData.filter(function (element) {
        return element !== undefined;
      });

      console.log("dummy data: ", dummyData);

      that.addCluster(dummyData);
    } else {
      for (var i = 0; i < array.length; i++) {
        if (array[i].status !== "Expired") {
          that.addMarkersFirst(array[i]);
        }
      }
    }
  }

  addMarkersFirst(obj) {
    let that = this;
    that.allData.markersArray = [];
    that.allData.markersArray1 = [];
    if (that.displayNames) {
      let that = this;
      /////////////// merge images using canvas
      let c: any = document.createElement("canvas");
      c.width = 100;
      c.height = 60;
      var ctx = c.getContext("2d");

      ctx.fillStyle = "#1c2f66";
      ctx.fillRect(0, 0, 100, 20);
      // ctx.strokeRect(0, 0, 100, 70);

      ctx.font = "9pt sans-serif";
      ctx.fillStyle = "white";
      ctx.fillText(obj.Device_Name, 12, 15);

      var img = c.toDataURL();
      that.allData.map.addMarker({
        // title: data.Device_Name,
        position: { lat: obj.last_lat, lng: obj.last_lng },
        icon: {
          url: that.getIconUrl(obj),
          size: {
            height: 40,
            width: 20
          }
        },
      }).then((marker: Marker) => {
        that.allData.markersArray.push({
          key: obj._id,
          mark: marker
        });

        that.allData.map.addMarker({
          position: { lat: obj.last_lat, lng: obj.last_lng },
          icon: {
            url: img,
            size: {
              height: 60,
              width: 100
            }
          },
        }).then((marker1: Marker) => {
          // that.allData.markersArray1 = marker1;
          that.allData.markersArray1.push({
            key: obj._id,
            mark: marker1
          });
          if (obj.last_lat !== 0 && obj.last_lng !== 0) {
            let last_location = {
              lat: obj.last_lat,
              long: obj.last_lng
            }
            that.getAddress(last_location);
          }
          if (obj.status === 'RUNNING') {
            that.getPingOperation(obj)
          }
        });
      });
    } else {
      that.allData.map.addMarker({
        title: obj.Device_Name,
        position: { lat: obj.last_lat, lng: obj.last_lng },
        icon: {
          url: that.getIconUrl(obj),
          size: {
            height: 40,
            width: 20
          }
        },
      }).then((marker: Marker) => {
        that.allData.markersArray.push({
          key: obj._id,
          mark: marker
        });
        if (obj.last_lat !== 0 && obj.last_lng !== 0) {
          let last_location = {
            lat: obj.last_lat,
            long: obj.last_lng
          }
          that.getAddress(last_location);
        }
        // debugger
        if (obj.status === 'RUNNING') {
          that.getPingOperation(obj)
        }
      });
    }
  }

  getPingOperation(pdata, center = false) {
    let that = this;
    that._io.emit('acc', pdata.Device_ID);
    that.socketChnl.push(pdata.Device_ID + 'acc');
    that._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
      if (d4 != undefined)
        (function (data) {
          if (data == undefined) {
            return;
          }
          console.log("on ping: ", data);
          if (data._id != undefined && data.last_location != undefined) {
            var key = data._id;
            that.impkey = data._id;
            that.deviceDeatils = data;
            that.otherValues(data);
            console.log("icon url: ", that.getIconUrl(data));
            if (data.last_location != undefined) {
              that.getAddress(data.last_location);
            }
            var strStr;
            if (that.allData[key]) {
              strStr = ' Address: ' + that.address + '\n Last Updated: ' + moment(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
              that.socketSwitch[key] = data;
              if (that.allData[key].mark !== undefined) {
                // that.allData[key].mark.setTitle();

                that.allData[key].mark.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
                  // alert('clicked');
                  setTimeout(() => {
                    that.allData[key].mark.hideInfoWindow();
                  }, 2000)
                });
                that.allData[key].mark.setSnippet(strStr);
                that.allData[key].mark.setIcon(that.getIconUrl(data));

                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                var temp = _.cloneDeep(that.allData[key].poly[1]);
                that.allData[key].poly[0] = _.cloneDeep(temp);
                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                if (that.displayNames) {
                  that.allData[key].mark1.setPosition(that.allData[key].poly[1]);
                  that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                } else {
                  that.liveTrackForSingleVehicle(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                }
              } else {
                return;
              }
            }
            else {
              debugger
              that.allData[key] = {};
              that.socketSwitch[key] = data;
              that.allData[key].poly = [];
              that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
              strStr = ' Address: ' + that.address + '\n Last Updated: ' + moment(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;

              if (that.displayNames) {
                for (var t = 0; t < that.allData.markersArray.length; t++) {
                  if (that.allData.markersArray[t].key === key && that.allData.markersArray[t].mark !== undefined) {
                    that.allData[key].mark = undefined;
                    that.allData[key].mark = that.allData.markersArray[t].mark;
                    // that.allData.markersArray = that.allData.markersArray.filter(function (element) {
                    //   return element.key === key;
                    // });

                    console.log("mark key foud")

                    for (var j = 0; j < that.allData.markersArray1.length; j++) {
                      if (that.allData.markersArray1[j].key === key && that.allData.markersArray1[j].mark !== undefined) {
                        that.allData[key].mark1 = undefined;
                        that.allData[key].mark1 = that.allData.markersArray1[j].mark;
                        // that.allData.markersArray1 = that.allData.markersArray1.filter(function (element) {
                        //   return element.key === key;
                        // });
                        console.log("mark1 key foud")

                        var speed = data.status == "RUNNING" ? data.last_speed : 0;
                        that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                      }
                    }
                  }
                }
              } else {
                for (var t = 0; t < that.allData.markersArray.length; t++) {
                  if (that.allData.markersArray[t].key === key && that.allData.markersArray[t].mark !== undefined) {
                    that.allData[key].mark = undefined;
                    that.allData[key].mark = that.allData.markersArray[t].mark;
                    that.allData.markersArray = that.allData.markersArray.filter(function (element) {
                      return element.key === key;
                    });
                    that.allData[key].mark.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                      .subscribe(e => {
                        if (that.selectedVehicle === undefined) {
                          that.getAddressTitle(that.allData[key].mark);
                        }
                      });
                    that.getAddress(data.last_location);
                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                    that.liveTrackForSingleVehicle(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                  }
                }
              }

            }
          }
        })(d4)
    })
  }
  ///////////// CORDOVA SQLite CODE ////////////////////////////

}
