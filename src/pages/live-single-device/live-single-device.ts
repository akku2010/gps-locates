import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController, IonicPage, ViewController, ToastController, AlertController, Events, Navbar } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as io from 'socket.io-client';
import * as moment from 'moment';
import * as _ from "lodash";
import { GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, GoogleMapsMapTypeId, Polyline, LatLngBounds, ILatLng, Circle, CircleOptions } from '@ionic-native/google-maps';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { DrawerState } from 'ion-bottom-drawer';
import { Subscription } from 'rxjs/Subscription';
import { mapStyle } from '../live/map-style.model';
import * as TinyURL from 'tinyurl';
import { NearbyVehicleModal } from './neraby-vehicle-modal/neraby-vehicle-modal';
declare var google;

@IonicPage()
@Component({
  selector: 'page-live',
  templateUrl: 'live-single-device.html',
})
export class LiveSingleDevice implements OnInit, OnDestroy {
  showDistDuration: boolean = false;

  streetviewButtonColor: string = '#196796';
  streetColor: string = '#fff';

  navigateButtonColor: string = '#196796'; //Default Color
  navColor: string = '#fff';

  policeButtonColor: string = '#196796';
  policeColor: string = '#fff';

  eyeBtnColor: string = "#196796";
  eyeColor: string = "#fff";

  petrolButtonColor: string = "#196796";
  petrolColor: string = '#fff';

  shouldBounce = true;
  dockedHeight = 80;
  // distanceTop = 200;
  distanceTop = 56;
  drawerState = DrawerState.Docked;
  // drawerState = DrawerState.Top;
  states = DrawerState;
  minimumHeight = 50;
  transition = '0.85s ease-in-out';

  drawerState1 = DrawerState.Docked;
  dockedHeight1 = 150;
  distanceTop1 = 378;
  minimumHeight1 = 0;

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};
  service: any;
  data: any = {};
  _io: any;
  socketSwitch: any = {};
  socketChnl: any = [];
  socketData: any = {};
  selectedFlag: any = { _id: '', flag: false };
  allData: any = {};
  userdetails: any;
  showBtn: boolean;
  SelectVehicle: string;
  selectedVehicle: any;
  titleText: any;
  portstemp: any;
  showActionSheet: boolean;
  gpsTracking: any;
  power: any;
  currentFuel: any;
  last_ACC: any;
  today_running: string;
  today_stopped: string;
  timeAtLastStop: string;
  distFromLastStop: any;
  lastStoppedAt: string;
  fuel: any;
  total_odo: any;
  todays_odo: any;
  vehicle_speed: any;
  liveVehicleName: any;
  onClickShow: boolean;
  address: any;
  isEnabled: boolean = false;
  liveDataShare: any;
  showShareBtn: boolean = false;
  resToken: any;
  mapData: any[];
  menuActive: boolean;
  motionActivity: string;
  mapHideTraffic: boolean = false;
  locateme: boolean = false;
  latlngCenter: any;
  last_ping_on: any;
  deviceDeatils: any = {};
  tempArray: any[] = [];
  mapTrail: boolean = false;
  showFooter: boolean;
  condition: string = 'gpsc';
  condition1: string = 'light';
  condition2: string = 'light';
  tttime: number;
  acModel: any;
  showaddpoibtn: boolean = false;
  impkey: any;
  btnString: string = "Create Trip";
  showIcon: boolean = false;
  _id: any;
  expectation: any = {};
  polyLines: any[] = [];
  marksArray: any[] = [];
  // shwBckBtn: boolean = false;
  recenterMeLat: any;
  recenterMeLng: any;
  mapKey: any;
  zoomLevel: number = 18;

  markersArray: any[] = [];
  devices: any[] = [];
  speed: number;
  latLngArray: any[] = [];
  fraction: number = 0;
  direction: number = 1;
  intevalId: any;
  navigateBtn: boolean = false;
  voltage: number;
  singleVehData: any = {};
  circleTimer: any;
  circleObj: Circle;
  temporaryMarker: any;
  temporaryInterval: any;
  tempreture: number;
  door: number;
  nearbyPolicesArray: any = [];
  nearbyPetrolArray: any = [];

  takeAction: any = "live";
  socketurl: string = "https://www.oneqlik.in";
  showStreetMap: boolean = false;
  total_days: any = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    public elementRef: ElementRef,
    public platform: Platform,
    private event: Events,
    public modalCtrl: ModalController,
    private socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public storage: Storage,
    public plt: Platform,
    // private launchNavigator: LaunchNavigator,
    // private nativePageTransitions: NativePageTransitions,
    // private viewCtrl: ViewController,
    private translate: TranslateService,
    private geocoderApi: GeocoderProvider
  ) {
    // Environment.setBackgroundColor("black");
    this.callBaseURL();
    this.calculate7Days();

    var selectedMapKey;
    if (localStorage.getItem('MAP_KEY') != null) {
      selectedMapKey = localStorage.getItem('MAP_KEY');
      if (selectedMapKey == this.translate.instant('Hybrid')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      } else if (selectedMapKey == this.translate.instant('Normal')) {
        this.mapKey = 'MAP_TYPE_NORMAL';
      } else if (selectedMapKey == this.translate.instant('Terrain')) {
        this.mapKey = 'MAP_TYPE_TERRAIN';
      } else if (selectedMapKey == this.translate.instant('Satellite')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      }
    } else {
      this.mapKey = 'MAP_TYPE_NORMAL';
    }
    this.event.subscribe("tripstatUpdated", (data) => {
      if (data) {
        this.checktripstat();
      }
    })
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> " + JSON.stringify(this.userdetails));

    if (navParams.get("device") != null) {
      this.deviceDeatils = navParams.get("device");
    }
    this.motionActivity = 'Activity';
    this.menuActive = false;

    // console.log("check @Input() activity: ", this.deviceData);
  }

  callBaseURL() {
    // debugger
    if (localStorage.getItem("ENTERED_BASE_URL") === null) {
      let url = "https://www.oneqlik.in/pullData/getUrlnew";
      this.apiCall.getSOSReportAPI(url)
        .subscribe((data) => {
          console.log("base url: ", data);
          if (data.url) {
            localStorage.setItem("BASE_URL", JSON.stringify(data.url));
          }
          if (data.socket) {
            localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
          }
          this.getSocketUrl();
        }, err => {
          this.getSocketUrl();
        });
    }
  }

  calculate7Days() {
    var d0 = new Date().toISOString();
    this.total_days.push(d0);
    this.showSelectedTabArray.push({
      "day": d0,
      "showSelectedTab": true
    })
    console.log("date before 1 days: ", d0);
    var d1 = new Date(Date.now() - 1 * 24 * 60 * 60 * 1000).toISOString();
    this.total_days.push(d1);
    this.showSelectedTabArray.push({
      "day": d1,
      "showSelectedTab": false
    })
    console.log("date before 2 days: ", d1);
    var d2 = new Date(Date.now() - 2 * 24 * 60 * 60 * 1000).toISOString();
    this.total_days.push(d2);
    this.showSelectedTabArray.push({
      "day": d2,
      "showSelectedTab": false
    })
    console.log("date before 3 days: ", d2)
    var d3 = new Date(Date.now() - 3 * 24 * 60 * 60 * 1000).toISOString();
    this.total_days.push(d3);
    this.showSelectedTabArray.push({
      "day": d3,
      "showSelectedTab": false
    })
    console.log("date before 4 days: ", d3)
    var d4 = new Date(Date.now() - 4 * 24 * 60 * 60 * 1000).toISOString();
    this.total_days.push(d4);
    this.showSelectedTabArray.push({
      "day": d4,
      "showSelectedTab": false
    })
    console.log("date before 5 days: ", d4)
    var d5 = new Date(Date.now() - 5 * 24 * 60 * 60 * 1000).toISOString();
    this.total_days.push(d5);
    this.showSelectedTabArray.push({
      "day": d5,
      "showSelectedTab": false
    })
    console.log("date before 6 days: ", d5)
    var d6 = new Date(Date.now() - 6 * 24 * 60 * 60 * 1000).toISOString();
    this.total_days.push(d6);
    this.showSelectedTabArray.push({
      "day": d6,
      "showSelectedTab": false
    })
    console.log("date before 7 days: ", d6);
    console.log(this.total_days);

  }

  selectedDate: any = [];
  showSelectedTabArray: any = [];
  changeData(data, index) {
    console.log("chnages data: ", data);
    if (this.sevenDaysData.length > 0) {

    } else {
      this.showString = 'start';
    }
    for (var i = 0; i < this.showSelectedTabArray.length; i++) {
      if (this.showSelectedTabArray[i].showSelectedTab === true) {
        this.showSelectedTabArray[i].showSelectedTab = false;
      }
    }
    this.showSelectedTabArray[index].showSelectedTab = true;
    this.selectedDate = data;
    this.get7daysData();
  }
  sevenDaysData: any = [];
  showString: string = 'start';
  get7daysData() {
    let _bUrl = this.apiCall.mainUrl + 'user_trip/StoppageLogs?date=' + this.selectedDate + '&device=' + this.deviceDeatils._id;
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(resp => {
        console.log("got response:- " + resp.result)
        this.today_running = this.parseMillisecondsIntoReadableTime(resp.today_running);
        this.today_stopped = this.parseMillisecondsIntoReadableTime(resp.today_stopped);
        this.todays_odo = this.meterIntoKelometer(resp.todays_odo);
        if (resp.result.length > 0) {
          this.showString = "";
          this.sevenDaysData = resp.result.reverse();
        } else {
          this.sevenDaysData = [];
          this.showString = 'No Data Found!';
        }
      },
        err => {
          this.showString = 'No Data Found!';
          console.log(" got error: ", err);
        });
  }

  meterIntoKelometer(value) {
    var km = value / 1000;
    return km.toFixed(1);
    // alert(km.toFixed(1) + " km"); // 1613.8 km
  }

  car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
  bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z"
  truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z'
  carIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [10, 20],
  };
  busIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [12.5, 12.5],
  };
  bikeIcon = {
    path: this.bike,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    anchor: [12.5, 12.5],
  };

  truckIcon = {
    path: this.truck,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    anchor: [10, 20],
  };

  userIcon = {
    path: this.truck,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    anchor: [12.5, 12.5],
  }

  ambulance = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [10, 20],
  };

  icons = {
    "car": this.carIcon,
    "bike": this.bikeIcon,
    "truck": this.truckIcon,
    "bus": this.busIcon,
    "user": this.userIcon,
    "jcb": this.carIcon,
    "tractor": this.carIcon,
    "ambulance": this.carIcon,
    "auto": this.carIcon
  }

  ngOnInit() {
    let that = this;
    that.tempArray = [];
    this.checktripstat();
  }
  getSocketUrl() {
    if (localStorage.getItem('SOCKET_URL') !== null) {
      this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
    }
    this._io = io.connect(this.socketurl + "/gps", { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
  }
  hideMe: boolean = false;
  resumeListener: Subscription = new Subscription();
  ionViewDidEnter() {
    localStorage.removeItem("SocketFirstTime");
    this.getDefaultUserSettings();

    this.selectedDate = new Date().toISOString();;
    this.get7daysData();

    this.navBar.backButtonClick = (ev: UIEvent) => {
      console.log('this will work in Ionic 3 +');
      if (this.allData.map) {
        this.allData.map.remove();
      }
      this.hideMe = true;
      this.navCtrl.pop({
        animate: true, animation: 'transition-ios', direction: 'back'
      });
    }

    // this._io = io.connect(this.socketurl + "/gps", { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    // this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });


    this.initMap();
    this.showShareBtn = true;
    var paramData = this.navParams.get("device");
    this.singleVehData = this.navParams.get("device");
    this.titleText = paramData.Device_Name;
    this.temp(paramData);
    this.showActionSheet = true;

  }


  initMap() {
    if (this.allData.map != undefined) {
      this.allData.map.remove();
    }
    this.allData = {};
    let mapOptions = {
      gestures: {
        rotate: false,
        tilt: false,
        compass: false
      },
      mapType: this.mapKey
    }
    this.allData.map = GoogleMaps.create('map_canvas_single_device', mapOptions);
  }

  measurementUnit: string = 'MKS';
  getDefaultUserSettings() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.userdetails._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
  }
  ionViewWillEnter() {
    // if (this.plt.is('ios')) {
    //   this.shwBckBtn = true;
    //   this.viewCtrl.showBackButton(false);
    // }

    this.platform.ready().then(() => {
      this.resumeListener = this.platform.resume.subscribe(() => {
        var today, Christmas;
        today = new Date();
        Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
        var diffMs = (today - Christmas); // milliseconds between now & Christmas
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        if (diffMins >= 5) {
          localStorage.removeItem("backgroundModeTime");
          this.alertCtrl.create({
            message: this.translate.instant('Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?'),
            buttons: [
              {
                text: this.translate.instant('YES PROCEED'),
                handler: () => {
                  this.refreshMe();
                }
              },
              {
                text: this.translate.instant('Back'),
                handler: () => {
                  this.navCtrl.setRoot('DashboardPage');
                }
              }
            ]
          }).present();
        }
      })
    })
  }

  ionViewWillLeave() {
    // this.allData.map.remove();
    this.platform.ready().then(() => {
      this.resumeListener.unsubscribe();
    })
  }

  ionViewDidLeave() {
    let that = this;
    that.takeAction = "live";
    if (that.circleTimer) {
      clearInterval(that.circleTimer);
    }
    if (that.intevalId) {
      clearInterval(that.intevalId);
    }
    for (var i = 0; i < that.socketChnl.length; i++)
      that._io.removeAllListeners(that.socketChnl[i]);
    that._io.on('disconnect', () => {
      that._io.open();
    })
    if (localStorage.getItem("livepagetravelDetailsObject") != null) {
      localStorage.removeItem("livepagetravelDetailsObject");
    }
    localStorage.removeItem("gotPing");

    //////
    // that.allData.map.setVisible(false);
    // that.allData.map.setDiv(null);
  }

  ngOnDestroy() {
    // let that = this;
    // if (that.circleTimer) {
    //   clearInterval(that.circleTimer);
    // }
    // if (that.intevalId) {
    //   clearInterval(that.intevalId);
    // }
    // for (var i = 0; i < that.socketChnl.length; i++)
    //   that._io.removeAllListeners(that.socketChnl[i]);
    // that._io.on('disconnect', () => {
    //   that._io.open();
    // })
    // if (localStorage.getItem("livepagetravelDetailsObject") != null) {
    //   localStorage.removeItem("livepagetravelDetailsObject");
    // }
    // localStorage.removeItem("gotPing");
  }

  setDocHeight() {
    let that = this;
    that.distanceTop = 200;
    that.drawerState = DrawerState.Top;
  }

  setDocHeightAtFirst() {
    let that = this;
    that.distanceTop = 80;
    that.drawerState = DrawerState.Top;
  }

  segmentChanged(ev) {
    console.log(ev);
    this.takeAction = ev._value;

    if (this.takeAction === 'history') {
      this.navCtrl.push('HistoryDevicePage', {
        device: this.singleVehData
      })
    } else if (this.takeAction === 'trip') {
      this.navCtrl.push('TripReportPage', {
        param: this.singleVehData
      })
    } else if (this.takeAction === 'notif') {
      this.navCtrl.push('AllNotificationsPage', {
        param: this.singleVehData
      })
    } else if (this.takeAction === 'car_service') {
      this.navCtrl.push('MaintenanceReminderPage', {
        param: this.singleVehData
      })
    }
  }
  showOtherBtns: boolean = false;
  streetView() {
    if (this.plt.is('ios')) {
      this.showOtherBtns = !this.showOtherBtns;
    }
    if (this.streetviewButtonColor == '#d80622') {
      this.streetviewButtonColor = '#fff';
      this.streetColor = '#d80622';
    } else if (this.streetviewButtonColor == '#fff') {
      this.streetviewButtonColor = '#d80622';
      this.streetColor = '#fff';
    }
    let that = this;
    this.showStreetMap = !this.showStreetMap;
    if (this.showStreetMap) {
      document.getElementById("map_canvas_single_device").style.height = "50%";
      // debugger
      let fenway: any = {};
      if (this.singleVehData.last_location !== undefined) {
        fenway = { lat: this.singleVehData.last_location.lat, lng: this.singleVehData.last_location.long }
      } else if (this.singleVehData.last_lat !== undefined && this.singleVehData.last_lng !== undefined) {
        fenway = { lat: this.singleVehData.last_lat, lng: this.singleVehData.last_lng }
      }
      console.log("fenway: ", fenway);
      var panorama = new google.maps.StreetViewPanorama(
        document.getElementById("viewParanoma"),
        {
          position: fenway,
          pov: {
            heading: 34,
            pitch: 10
          }
        }
      );
      that.allData.map.setStreetView(panorama);

    } else {
      // document.getElementById('map_canvas_single_device').setAttribute("style", "height:99%");
      document.getElementById("map_canvas_single_device").style.height = "99%";
    }
  }

  showNearby() {
    let modal = this.modalCtrl.create(NearbyVehicleModal)
    modal.onDidDismiss((ddata) => {
      console.log("modal dissmissed!!")
      console.log("key is ", ddata);
      this.openMap(ddata);
    })
    modal.present();
  }

  openMap(data) {
    let coordinates = {
      lat: this.recenterMeLat,
      long: this.recenterMeLng
    }
    this.getOnlyArea(coordinates, data);

  }

  addNearbyMarkers(key, url, icurl) {

    console.log("google maps activity: ", url);
    this.apiCall.getSOSReportAPI(url).subscribe(resp => {
      console.log(resp.status);
      if (resp.status === 'OK') {
        console.log(resp.results);
        console.log(resp);
        var mapData = resp.results.map(function (d) {
          return { lat: d.geometry.location.lat, lng: d.geometry.location.lng };
        })

        let bounds = new LatLngBounds(mapData);

        let that = this;
        for (var t = 0; t < resp.results.length; t++) {
          this.allData.map.addMarker({
            title: resp.results[t].name,
            position: {
              lat: resp.results[t].geometry.location.lat,
              lng: resp.results[t].geometry.location.lng
            },
            icon: {
              url: icurl,
              size: {
                height: 35,
                width: 35
              }
            }
          }).then((mark: Marker) => {
            if (key === 'police') {
              that.nearbyPolicesArray.push(mark);
            } else if (key === 'petrol') {
              that.nearbyPetrolArray.push(mark);
            }
          });
        }
        this.allData.map.moveCamera({
          target: bounds
        });
        this.allData.map.setPadding(20, 20, 20, 20);
        this.zoomLevel = 15;
      } else if (resp.status === 'ZERO_RESULTS') {
        if (key === 'police') {
          this.showToastMsg('No nearby police stations found.');
        } else if (key === 'petrol') {
          this.showToastMsg('No nearby petrol pump found.');
        }
      } else if (resp.status === 'OVER_QUERY_LIMIT') {
        this.showToastMsg('Query limit exeed. Please try after some time.');
      } else if (resp.status === 'REQUEST_DENIED') {
        this.showToastMsg('Please check your API key.');
      } else if (resp.status === 'INVALID_REQUEST') {
        this.showToastMsg('Invalid request. Please try again.');
      } else if (resp.status === 'UNKNOWN_ERROR') {
        this.showToastMsg('An unknown error occured. Please try again.');
      }
    })
  }

  showToastMsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    }).present();
  }
  @ViewChild(Navbar) navBar: Navbar;
  goBack() {
    // debugger
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop({
        animate: true, animation: 'transition-ios', direction: 'forward'
      })
    }
  }

  // goBack() {
  //   if (this.navCtrl.canGoBack()) {
  //     let options: NativeTransitionOptions = {
  //       direction: 'left',
  //       duration: 500,
  //       slowdownfactor: 3,
  //       slidePixels: 20,
  //       iosdelay: 100,
  //       androiddelay: 150,
  //       fixedPixelsTop: 0,
  //       fixedPixelsBottom: 60

  //     };

  //     this.nativePageTransitions.slide(options);
  //     this.navCtrl.pop();
  //   } else {
  //     let options: NativeTransitionOptions = {
  //       duration: 700
  //     };
  //     this.nativePageTransitions.fade(options);
  //     this.navCtrl.setRoot('DashboardPage');
  //   }
  // }

  refreshMe() {
    this.ngOnDestroy();
    this.ngOnInit();
    this.ionViewDidEnter();
  }

  refreshMe1() {
    this.ionViewDidLeave();
    // this.ngOnDestroy();
    this._io = io.connect(this.socketurl + '/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    var paramData = this.navParams.get("device");
    this.socketInit(paramData);
  }

  reCenterMe() {
    // this.allData.map.animateCamera({
    //   target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
    //   // zoom: 15,
    //   tilt: 30,
    //   // bearing: this.allData.map.getCameraBearing(),
    //   duration: 1800,
    //   // setCompassEnabled: false
    // }).then(() => {

    // })

    this.allData.map.moveCamera({
      target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
      // zoom: 15,
      tilt: 30,
      // bearing: this.allData.map.getCameraBearing(),
      duration: 1800,
      // setCompassEnabled: false
    }).then(() => {

    })
  }

  // newMap() {
  //   let mapOptions = {
  //     camera: { zoom: 10 },
  //     gestures: {
  //       'rotate': false,
  //       'tilt': true,
  //       'scroll': true
  //     },
  //     gestureHandling: 'cooperative'
  //   };
  //   let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
  //   // Environment.setBackgroundColor("black");
  //   return map;
  // }

  isNight() {
    //Returns true if the time is between
    //7pm to 5am
    let time = new Date().getHours();
    return (time > 5 && time < 19) ? false : true;
  }

  checktripstat() {
    var url = this.apiCall.mainUrl + "user_trip/getLastTrip?device=" + this.deviceDeatils._id + "&status=Started&tripInfo=last_trip";

    this.apiCall.getSOSReportAPI(url)
      .subscribe(data => {
        // debugger
        if (!data.message) {
          this.allData.tripStat = data[0].trip_status;
          if (this.allData.tripStat == 'Started') {
            var sources = new LatLng(parseFloat(data[0].start_lat), parseFloat(data[0].start_long));
            var dest = new LatLng(parseFloat(data[0].end_lat), parseFloat(data[0].end_long));
            this.calcRoute(sources, dest);
          }
        }
      })
  }

  navigateFromCurrentLoc() {
    if (this.plt.is('ios')) {
      window.open("https://www.google.com/maps/dir/?api=1&destination=" + this.recenterMeLat + "," + this.recenterMeLng + "&travelmode=driving", "_self");

    } else {
      if (this.plt.is('android')) {
        window.open("https://www.google.com/maps/dir/?api=1&destination=" + this.recenterMeLat + "," + this.recenterMeLng + "&travelmode=driving", "_blank");

      }
    }
  }

  calcRoute(start, end) {
    this.allData.AIR_PORTS = [];
    var directionsService = new google.maps.DirectionsService();
    let that = this;
    var request = {
      origin: start,
      destination: end,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING
    };

    directionsService.route(request, function (response, status) {

      if (status == google.maps.DirectionsStatus.OK) {
        var path = new google.maps.MVCArray();
        for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
          path.push(response.routes[0].overview_path[i]);

          that.allData.AIR_PORTS.push({
            lat: path.g[i].lat(), lng: path.g[i].lng()
          });
          // debugger
          if (that.allData.AIR_PORTS.length > 1) {
            if (!that.navigateBtn) {
              that.allData.map.addMarker({
                title: 'My Location',
                position: start,
                icon: 'green'
              }).then((mark: Marker) => {
                that.marksArray.push(mark);
              })
              that.allData.map.addMarker({
                title: 'Destination',
                position: end,
                icon: 'red'
              }).then((mark: Marker) => {
                that.marksArray.push(mark);
              })
            }
            // that.allData.map.setPadding(0, 30, 0, 55)
            that.allData.map.addPolyline({
              'points': that.allData.AIR_PORTS,
              'color': '#3b3a3a',
              'width': 4,
              'geodesic': true,
            }).then((poly: Polyline) => {
              that.polyLines.push(poly);
              that.getTravelDetails(start, end);
            })
          }
        }

      }
    });
  }

  getTravelDetails(source, dest) {
    this.service = new google.maps.DistanceMatrixService();
    let that = this;
    this._id = setInterval(() => {
      if (localStorage.getItem("livepagetravelDetailsObject") != null) {
        if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
          that.expectation = JSON.parse(localStorage.getItem("livepagetravelDetailsObject"));
          console.log("expectation: ", that.expectation)
          that.showDistDuration = true;
        } else {
          clearInterval(this._id);
        }
      }
    }, 3000)
    that.service.getDistanceMatrix({
      origins: [source],
      destinations: [dest],
      travelMode: 'DRIVING'
    }, that.callback);
    // that.apiCall.stopLoading();
  }

  callback(response, status) {
    let travelDetailsObject;
    if (status == 'OK') {
      var origins = response.originAddresses;
      for (var i = 0; i < origins.length; i++) {
        var results = response.rows[i].elements;
        for (var j = 0; j < results.length; j++) {
          var element = results[j];
          var distance = element.distance.text;
          var duration = element.duration.text;
          travelDetailsObject = {
            distance: distance,
            duration: duration
          }
        }
      }
      localStorage.setItem("livepagetravelDetailsObject", JSON.stringify(travelDetailsObject));
    }
  }

  diff(start, end) {
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);

    // If using time pickers with 24 hours format, add the below line get exact hours
    if (hours < 0)
      hours = hours + 24;

    return (hours <= 9 ? "0" : "") + hours + " hours " + (minutes <= 9 ? "0" : "") + minutes + " mins";
  }

  callendtripfunc(res) {
    var endtime = moment(new Date(), 'DD/MM/YYY').format('hh:mm');
    var starttime = moment(new Date(res.doc.start_time), 'DD/MM/YYYY').format('hh:mm');
    var duration = this.diff(starttime, endtime);
    var dest = new LatLng(res.doc.start_lat, res.doc.start_long);
    var dest2 = new LatLng(res.doc.end_lat, res.doc.end_long);
    var distance = Spherical.computeDistanceBetween(dest, dest2); // in meters

    var bUrl = this.apiCall.mainUrl + "user_trip/updatePlantrip";
    var payload = {
      "user": res.doc.user,
      "device": res.doc.device,
      "start_loc": {
        "lat": res.doc.start_lat,
        "long": res.doc.start_long
      },
      "end_loc": {
        "lat": res.doc.end_lat,
        "long": res.doc.end_long,
      },
      "duration": duration,
      "distance": distance,
      "trip_status": "completed",
      "trip_id": res.doc._id,
      "end_time": new Date().toISOString()
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(bUrl, payload)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("tri end data: ", data)
        // alert("trip ended succesfully");
        let toast = this.toastCtrl.create({
          message: 'Trip has ended succesfully.',
          duration: 1500,
          position: 'bottom'
        })
        toast.present();
        let that = this;
        // debugger
        that.allData.tripStat = 'completed';
        if (that.polyLines.length > 0) {
          for (var t = 0; t < that.polyLines.length; t++) {
            that.polyLines[t].remove();
          }
        }

        if (that.marksArray.length > 0) {
          for (var e = 0; e < that.marksArray.length; e++) {
            that.marksArray[e].remove();
          }
        }
        if (localStorage.getItem("livepagetravelDetailsObject") != null) {
          localStorage.removeItem("livepagetravelDetailsObject");
        }
        this.checktripstat();
      })

  }

  endTrip() {
    let alert = this.alertCtrl.create({
      title: this.translate.instant('Confirm'),
      message: this.translate.instant('Do you want to end the trip?'),
      buttons: [
        {
          text: this.translate.instant('YES PROCEED'),
          handler: () => {
            this.storage.get("TRIPDATA").then((res) => {
              if (res) {
                this.callendtripfunc(res);
              }
            })
          }
        },
        {
          text: this.translate.instant('Back')
        }
      ]

    })
    alert.present()
  }
  // onClickMap(maptype) {
  //   let that = this;
  //   if (maptype == 'SATELLITE') {
  //     that.allData.map.setMapTypeId(GoogleMapsMapTypeId.SATELLITE);
  //   } else {
  //     if (maptype == 'TERRAIN') {
  //       that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
  //     } else {
  //       if (maptype == 'NORMAL') {
  //         that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
  //       } else {
  //         if (maptype == 'HYBRID') {
  //           that.allData.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
  //         }
  //       }
  //     }
  //   }
  // }

  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE' || maptype == 'HYBRID') {
      // this.ngOnDestroy();
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
    } else {
      if (maptype == 'TERRAIN') {
        // this.ngOnDestroy();
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
      } else {
        if (maptype == 'NORMAL') {
          // this.ngOnDestroy();
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
        }
      }
    }
  }

  createTrip() {
    console.log("create trip data: ", this.deviceDeatils)
    this.navCtrl.push('CreateTripPage', {
      'paramData': this.deviceDeatils,
    })
  }

  trafficFunc() {
    let that = this;
    that.isEnabled = !that.isEnabled;
    if (that.isEnabled == true) {
      that.allData.map.setTrafficEnabled(true);
    } else {
      that.allData.map.setTrafficEnabled(false);
    }
  }

  shareLive() {
    let that = this;
    that.showActionSheet = false;
    // that.drawerHidden = false;
    that.drawerState1 = DrawerState.Docked;
    that.showFooter = true;
  }

  sharedevices(param) {
    let that = this;
    if (param == '15mins') {
      that.condition = 'gpsc';
      that.condition1 = 'light';
      that.condition2 = 'light';
      that.tttime = 15;
      // that.tttime  = (15 * 60000); //for miliseconds
    } else {
      if (param == '1hour') {
        that.condition1 = 'gpsc';
        that.condition = 'light';
        that.condition2 = 'light';
        that.tttime = 60;
        // that.tttime  = (1 * 3600000); //for miliseconds
      } else {
        if (param == '8hours') {
          that.condition2 = 'gpsc';
          that.condition = 'light';
          that.condition1 = 'light';
          that.tttime = (8 * 60);
          // that.tttime  = (8 * 3600000);
        }
      }
    }
  }

  shareLivetemp() {
    let that = this;
    if (that.tttime == undefined) {
      that.tttime = 15;
    }
    var data = {
      id: that.liveDataShare._id,
      imei: that.liveDataShare.Device_ID,
      sh: this.userdetails._id,
      ttl: that.tttime   // set to 1 hour by default
    };
    this.apiCall.startLoading().present();
    this.apiCall.shareLivetrackCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.resToken = data.t;
        this.liveShare();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  liveShare() {
    let that = this;
    var link = 'https://www.oneqlik.in/' + "share/liveShare?t=" + that.resToken;
    TinyURL.shorten(link).then((res) => {
      // alert("tinyurl: " + res);
      that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", res);

    })
    // that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
    that.showActionSheet = true;
    that.showFooter = false;
    that.tttime = undefined;
  }

  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    // var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
    var h = absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    // return h + ':' + m;
    // return h + ':' + m + ':' + s;
    // return h + 'h ' + m + 'min';
    if (h === 0) {
      return m + 'min';
    } else {
      if (m === '00') {
        return '0min';
      } else {
        return h + 'h ' + m + 'min';
      }
    }


  }

  callObjFunc(d) {
    let that = this;
    var _bUrl = that.apiCall.mainUrl + 'devices/getDevicebyId?deviceId=' + d.Device_ID;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        console.log("updated device object=> " + resp);
        if (!resp) {
          return;
        } else {
          that.deviceDeatils = resp;
        }
      })
  }

  settings() {
    let that = this;
    let profileModal = this.modalCtrl.create('DeviceSettingsPage', {
      param: that.deviceDeatils
    });
    profileModal.present();

    profileModal.onDidDismiss(() => {
      that.callObjFunc(that.deviceDeatils);

    })
  }
  areaName: any;
  getOnlyArea(coordinates, data) {
    let that = this;
    if (!coordinates) {
      that.address = 'N/A';
      return;
    }
    this.geocoderApi.getAreaName(coordinates.lat, coordinates.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.saveAddressToServer(str, coordinates.lat, coordinates.long);
        that.areaName = str;

        let url = "https://www.google.com/maps/search/?api=1&query=" + data + "+" + that.areaName;
        // window.open("https://www.google.com/maps/search/?api=1&query=" + data, "_self");
        window.open(url, "_self");
      })
  }

  getAddress(coordinates) {
    let that = this;
    if (!coordinates) {
      that.address = 'N/A';
      return;
    }
    let tempcord = {
      "lat": coordinates.lat,
      "long": coordinates.long
    }
    this.apiCall.getAddress(tempcord)
      .subscribe(res => {
        // console.log("test");
        // console.log("address result", res);
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.saveAddressToServer(str, coordinates.lat, coordinates.long);
              that.address = str;
              console.log("inside", that.address);
            })
        } else {
          that.address = res.address;
        }
        // console.log(that.address);
      },
        err => {
          this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.saveAddressToServer(str, coordinates.lat, coordinates.long);
              that.address = str;
              console.log("inside", that.address);
            })
        });
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apiCall.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        // console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

  weDidntGetPing(data) {
    console.log("I came here in weDidntGetPing function")
    let that = this;
    if (data._id != undefined && data.last_location != undefined) {
      that.impkey = data._id;
      that.otherValues(data);
      if (data.last_location != undefined) {
        console.log("we did not get pung: ", that.getIconUrl(data))
        // debugger
        console.log("check lat: ", data.last_location.lat)
        console.log("check long: ", data.last_location.long)
        that.allData.map.addMarker({
          title: data.Device_Name,
          position: { lat: data.last_location.lat, lng: data.last_location.long },
          icon: {
            url: that.getIconUrl(data),
            size: {
              height: 40,
              width: 20
            }
          },
        }).then((marker: Marker) => {
          // debugger
          that.temporaryMarker = marker;
          // that.allData[key].mark = marker;
          marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
            .subscribe(e => {
              that.liveVehicleName = data.Device_Name;
              that.showActionSheet = true;
              that.showaddpoibtn = true;

              that.getAddressTitle(marker);

            });
          that.getAddress(data.last_location);
          that.socketInit(data);

        });
        // }
      }
    } else if (data._id != undefined && (data.last_lat !== undefined && data.last_lng !== undefined)) {
      that.impkey = data._id;
      that.otherValues(data);
      // if (data.last_location != undefined) {
      console.log("something went wrong: ", that.getIconUrl(data))
      // debugger
      // if (localStorage.getItem('gotPing') === null) {
      that.allData.map.addMarker({
        title: data.Device_Name,
        position: { lat: data.last_lat, lng: data.last_lng },
        icon: {
          url: that.getIconUrl(data),
          size: {
            height: 40,
            width: 20
          }
        },
      }).then((marker: Marker) => {
        // debugger
        that.temporaryMarker = marker;
        // that.allData[key].mark = marker;
        marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
          .subscribe(e => {
            that.liveVehicleName = data.Device_Name;
            that.showActionSheet = true;
            that.showaddpoibtn = true;

            that.getAddressTitle(marker);

          });
        let last_location = {
          lat: data.last_lat,
          long: data.last_lng
        }
        that.getAddress(last_location);
        that.socketInit(data);

      });
      // }
      // }
    }
  }
  socketInit(pdata, center = false) {

    this._io.emit('acc', pdata.Device_ID);
    this.socketChnl.push(pdata.Device_ID + 'acc');
    let that = this;
    this._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
      if (d4 != undefined)
        console.log("ping data: => ", d4);
      (function (data) {
        if (data == undefined) {
          return;
        }
        localStorage.setItem("pdata", JSON.stringify(data))
        that.socketData = data;
        if (data._id != undefined && data.last_location != undefined) {
          var key = data._id;
          that.impkey = data._id;
          that.deviceDeatils = data;

          console.log("status: " + data.status.toLowerCase());
          console.log("last speed: " + data.last_speed)

          console.log(that.getIconUrl(data));

          that.otherValues(data);
          if (that.allData[key]) {
            that.socketSwitch[key] = data;
            console.log("check mark is avalible or not? ", that.allData[key].mark)
            if (that.allData[key].mark !== undefined) {
              that.allData[key].mark.setIcon(that.getIconUrl(data));
              // that.allData[key].mark.setIcon(ic);
              that.allData[key].mark.setPosition(that.allData[key].poly[1]);
              var temp = _.cloneDeep(that.allData[key].poly[1]);
              that.allData[key].poly[0] = _.cloneDeep(temp);
              that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

              var speed = data.status == "RUNNING" ? data.last_speed : 0;
              that.getAddress(data.last_location);
              that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 100, center, data._id, that);
            } else {
              return;
            }
          }
          else {
            that.allData[key] = {};
            that.socketSwitch[key] = data;
            that.allData[key].poly = [];
            if (data.status === 'STOPPED' || data.status === 'OUT OF REACH') {

            } else {
              console.log("check status: ", data.status)
              if (data.sec_last_location) {
                that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
              } else {
                that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
              }
            }

            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
            if (data.last_location != undefined) {
              //////////////////////
              if (that.temporaryMarker !== undefined) {
                that.temporaryMarker.setTitle(data.Device_Name);
                that.temporaryMarker.setPosition(that.allData[key].poly[0]);
                that.allData.map.animateCamera({
                  target: {
                    lat: that.allData[key].poly[0].lat,
                    lng: that.allData[key].poly[0].lng
                  },
                  duration: 1800
                })

                that.allData[key].mark = that.temporaryMarker;
                ///////////////////////////////
                // localStorage.setItem("SocketFirstTime", "SocketFirstTime");
                // that.temporaryMarker.remove();
                that.allData[key].mark.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                  .subscribe(e => {
                    that.liveVehicleName = data.Device_Name;
                    that.showActionSheet = true;
                    that.showaddpoibtn = true;

                    that.getAddressTitle(that.allData[key].mark);

                  });
                that.getAddress(data.last_location);
                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                // that.liveTrack(that.allData.map, that.allData[key].mark, ic.url, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 100, center, data._id, that);

              } else {
                that.allData.map.addMarker({
                  title: data.Device_Name,
                  position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                  icon: {
                    url: that.getIconUrl(data),
                    size: {
                      width: 20,
                      height: 40
                    }
                  },
                }).then((marker: Marker) => {

                  that.allData[key].mark = marker;
                  marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                    .subscribe(e => {
                      that.liveVehicleName = data.Device_Name;
                      that.showActionSheet = true;
                      that.showaddpoibtn = true;

                      that.getAddressTitle(marker);

                    });
                  that.getAddress(data.last_location);
                  var speed = data.status == "RUNNING" ? data.last_speed : 0;
                  that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 100, center, data._id, that);
                })
              }

            }
          }
        }
      })(d4)
    })
  }

  // socketInit(pdata, center = false) {
  //   this.allData.allKey = [];
  //   // let that = this;
  //   this._io.emit('acc', pdata.Device_ID);
  //   this.socketChnl.push(pdata.Device_ID + 'acc');
  //   this._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
  //     console.log("live data", d1, d2, d3, d4)
  //     let that = this;

  //     if (d4 != undefined)
  //       (function (data) {

  //         if (data == undefined) {
  //           return;
  //         }
  //         //  console.log( that.socketSwitch);
  //         if (data._id != undefined && data.last_location != undefined) {
  //           var key = data._id;
  //           that.impkey = data._id;
  //           let ic = _.cloneDeep(that.icons[data.iconType]);
  //           if (!ic) {
  //             console.warn(data.Device_ID + " has no iconType", data.iconType);
  //             return;
  //           }
  //           ic.path = null;
  //           // ic.url = 'https://www.oneqlik.in/images/' + data.status.toLowerCase() + data.iconType + '.png#' + data._id;
  //           ic.url = that.getIconUrl(data);

  //           that.otherValues(data);
  //           // if (localStorage.getItem("SocketFirstTime") !== null) {
  //           //   that.reCenterMe();
  //           // }

  //           if (that.allData[key]) {
  //             that.socketSwitch[key] = data;

  //             if (that.allData[key].mark !== undefined) {
  //               // that.allData[key].mark.setPosition(that.allData[key].poly[1]);
  //               that.allData[key].mark.setIcon(that.getIconUrl(data));
  //               var temp = _.cloneDeep(that.allData[key].poly[1]);
  //               that.allData[key].poly[0] = _.cloneDeep(temp);
  //               that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
  //               that.getAddress(data.last_location);
  //               var speed = data.status == "RUNNING" ? data.last_speed : 0;
  //               that.allData[key].mark.setPosition(that.allData[key].poly[1]);
  //               that.liveTrack(that.allData.map, that.allData[key].mark, ic.url, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
  //             }
  //           }
  //           else {
  //             that.allData[key] = {};
  //             that.socketSwitch[key] = data;
  //             that.allData.allKey.push(key);
  //             that.allData[key].poly = [];
  //             if (data.sec_last_location) {
  //               that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
  //             } else {
  //               that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
  //             }
  //             that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
  //             if (data.last_location != undefined) {
  //               if (that.temporaryMarker !== undefined) {
  //                 that.temporaryMarker.setTitle(data.Device_Name);
  //                 that.temporaryMarker.setPosition(that.allData[key].poly[0]);
  //                 that.allData.map.moveCamera({
  //                   target: {
  //                     lat: that.allData[key].poly[0].lat,
  //                     lng: that.allData[key].poly[0].lng
  //                   },
  //                   duration: 1800
  //                 })

  //                 that.allData[key].mark = that.temporaryMarker;
  //                 ///////////////////////////////
  //                 // localStorage.setItem("SocketFirstTime", "SocketFirstTime");
  //                 // that.temporaryMarker.remove();
  //                 that.allData[key].mark.addEventListener(GoogleMapsEvent.MARKER_CLICK)
  //                   .subscribe(e => {
  //                     that.liveVehicleName = data.Device_Name;
  //                     that.showActionSheet = true;
  //                     that.showaddpoibtn = true;

  //                     that.getAddressTitle(that.allData[key].mark);

  //                   });
  //                 that.getAddress(data.last_location);
  //                 var speed = data.status == "RUNNING" ? data.last_speed : 0;
  //                 that.liveTrack(that.allData.map, that.allData[key].mark, ic.url, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
  //               } else {
  //                 that.allData.map.addMarker({
  //                   title: data.Device_Name,
  //                   position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
  //                   icon: {
  //                     url: that.getIconUrl(data),
  //                     size: {
  //                       height: 40,
  //                       width: 20
  //                     }
  //                   },
  //                 }).then((marker: Marker) => {

  //                   that.allData[key].mark = marker;
  //                   marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
  //                     .subscribe(e => {
  //                       that.liveVehicleName = data.Device_Name;
  //                       that.showActionSheet = true;
  //                       that.showaddpoibtn = true;

  //                       that.getAddressTitle(marker);

  //                     });
  //                   that.getAddress(data.last_location);
  //                   var speed = data.status == "RUNNING" ? data.last_speed : 0;
  //                   that.liveTrack(that.allData.map, that.allData[key].mark, ic.url, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
  //                 });
  //               }

  //               // var infobox = new InfoBox({
  //               //   content: "<div class='infobox' style='padding:2px'><small><b style='color:" + that.getClr(data.status) + "'>" + data.Device_Name + "</b></small></div>",
  //               //   disableAutoPan: false,
  //               //   maxWidth: 150,
  //               //   alignBottom: true,
  //               //   pixelOffset: new google.maps.Size(-25, -20),
  //               //   zIndex: null,
  //               //   boxStyle: {
  //               //     opacity: 1,
  //               //     zIndex: 999,
  //               //     width: "auto",
  //               //     padding: "2px"
  //               //   },
  //               //   //closeBoxMargin: "12px 4px 2px 2px",
  //               //   closeBoxURL: "",
  //               //   infoBoxClearance: new google.maps.Size(1, 1)
  //               // });
  //               // that.data[key].infobox = infobox;
  //               // infobox.open(that.data.map, that.data[key].mark)
  //               // that.info(that.data[key].mark, function (e) {
  //               //   that.data.map.setCenter(e.latLng);
  //               //   that.socketSwitch.selected = data._id;
  //               // })
  //             }
  //             // var speed = data.status == "RUNNING" ? data.last_speed : 0;
  //             // that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
  //           }
  //         }
  //       })(d4)

  //   })
  // }

  addCircleWithAnimation(lat, lng) {
    let that = this;
    var _radius = 500;
    var rMin = _radius * 1 / 9;
    var rMax = _radius;
    var direction = 1;
    let GOOGLE: ILatLng = {
      "lat": lat,
      "lng": lng
    };

    let circleOption: CircleOptions = {
      'center': GOOGLE,
      'radius': 500,
      'fillColor': 'rgb(216, 6, 34)',
      'fillOpacity': 0.6,
      'strokeColor': '#950417',
      'strokeOpacity': 1,
      'strokeWidth': 0.5
    };
    // Add circle

    let circle: Circle = that.allData.map.addCircleSync(circleOption);
    that.circleObj = circle;

    that.circleTimer = setInterval(() => {
      var radius = circle.getRadius();

      if ((radius > rMax) || (radius < rMin)) {
        direction *= -1;
      }
      var _par = (radius / _radius) - 0.1;
      var opacity = 0.4 * _par;
      var colorString = that.RGBAToHexA(216, 6, 34, opacity);
      circle.setFillColor(colorString);
      circle.setRadius(radius + direction * 10);
    }, 30);
  }

  RGBAToHexA(r, g, b, a) {
    r = r.toString(16);
    g = g.toString(16);
    b = b.toString(16);
    a = Math.round(a * 255).toString(16);

    if (r.length == 1)
      r = "0" + r;
    if (g.length == 1)
      g = "0" + g;
    if (b.length == 1)
      b = "0" + b;
    if (a.length == 1)
      a = "0" + a;

    return "#" + r + g + b + a;
  }

  getAddressTitle(marker) {
    this.geocoderApi.reverseGeocode(marker.getPosition().lat, marker.getPosition().lng)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        marker.setSnippet(str);
      })
  }

  addPOI() {
    let that = this;
    let modal = this.modalCtrl.create(PoiPage, {
      param1: that.allData[that.impkey]
    });
    modal.onDidDismiss((data) => {
      console.log(data)
      let that = this;
      that.showaddpoibtn = false;
    });
    modal.present();
    console.log("param data", "param1");
  }

  /////////// Anjali code ////////

  getIconUrl(data) {
    let that = this;
    var iconUrl;
    debugger
    if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0)) {
      if (that.plt.is('ios')) {
        iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
      } else if (that.plt.is('android')) {
        iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
      }
    } else {
      if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0)) {
        if (that.plt.is('ios')) {
          iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
        } else if (that.plt.is('android')) {
          iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
        }
      } else {
        if ((data.status.toLowerCase() === 'stopped') && Number(data.last_speed) > 0) {
          if (that.plt.is('ios')) {
            iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
          } else if (that.plt.is('android')) {
            iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
          }
        } else {
          var stricon;
          if (data.status.toLowerCase() === 'out of reach') {
            stricon = "outofreach";
            if (that.plt.is('ios')) {
              iconUrl = 'www/assets/imgs/vehicles/' + stricon + data.iconType + '.png';
            } else if (that.plt.is('android')) {
              iconUrl = './assets/imgs/vehicles/' + stricon + data.iconType + '.png';
            }
          } else {
            if (that.plt.is('ios')) {
              iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
            } else if (that.plt.is('android')) {
              iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
            }
          }
        }
      }
    }
    console.log("icon url: ", iconUrl);
    return iconUrl;
  }
  battery_percent: any;
  otherValues(data) {
    let that = this;
    that.todays_odo = 0;

    that.vehicle_speed = (data.last_speed ? data.last_speed : 0);

    that.battery_percent = (data.battery_percent ? data.battery_percent : 0);
    that.todays_odo = (data.today_odo ? data.today_odo : 0);
    that.total_odo = (data.total_odo ? data.total_odo : 0);
    if (that.userdetails.fuel_unit == 'LITRE') {
      that.fuel = (data.currentFuel ? data.currentFuel : null);
    } else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
      that.fuel = (data.fuel_percent ? data.fuel_percent : null);
    } else {
      that.fuel = (data.currentFuel ? data.currentFuel : null);
    }

    that.voltage = (data.battery ? data.battery : 0);

    if (data.last_location) {
      that.recenterMeLat = data.last_location.lat;
      that.recenterMeLng = data.last_location.long;
      that.getAddress(data.last_location);
    } else {
      if (data.last_lat !== undefined && data.last_lng !== undefined) {
        let location = {
          lat: data.last_lat,
          long: data.last_lng
        }
        that.recenterMeLat = data.last_lat;
        that.recenterMeLng = data.last_lng;
        that.getAddress(location);
      }
    }

    that.last_ping_on = (data.last_ping_on ? data.last_ping_on : 'N/A');

    var tempvar = new Date(data.lastStoppedAt);
    if (data.lastStoppedAt != null) {
      var fd = tempvar.getTime();
      var td = new Date().getTime();
      var time_difference = td - fd;
      var total_min = time_difference / 60000;
      var hours = total_min / 60
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      that.lastStoppedAt = rhours + ':' + rminutes;
    } else {
      that.lastStoppedAt = '00' + ':' + '00';
    }

    that.distFromLastStop = data.distFromLastStop;
    if (!isNaN(data.timeAtLastStop)) {
      that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
    } else {
      that.timeAtLastStop = '00:00:00';
    }

    that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
    that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
    that.last_ACC = data.last_ACC;
    that.acModel = data.ac;
    that.currentFuel = data.currentFuel;
    that.power = data.power;
    that.gpsTracking = data.gpsTracking;
    that.tempreture = data.temp;
    that.door = data.door;

  }

  createMarker(location, indice, iconUrl) {
    let latlng = new LatLng(location.last_location.lat, location.last_location.long);
    let markerOptions = {
      title: 'Ionic',
      animation: 'BOUNCE',
      position: latlng,
      icon: iconUrl
    };
    this.allData.map.addMarker(markerOptions).then((marker) => {
      this.markersArray[indice] = marker;
      console.log('merkers array list: ', this.markersArray);
    });
  }

  liveTrack(map, mark, coords, speed, delay, center, id, that) {
    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);
    if (center) {
      map.setCameraTarget(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
        // mark.setIcon(icons);
      }
      function _moveMarker() {
        // debugger
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {

          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.mapTrail) {
            that.showTrail(lat, lng, map);
          } else {
            if (that.allData.polylineID) {
              that.tempArray = [];
              that.allData.polylineID.remove();
            }
          }
          map.setCameraTarget(new LatLng(lat, lng));
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.mapTrail) {
            that.showTrail(dest.lat, dest.lng, map);
          }
          else {
            if (that.allData.polylineID) {
              that.tempArray = [];
              that.allData.polylineID.remove();
            }
          }
          map.setCameraTarget(dest);
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  // liveTrack(map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
  //   var target = 0;
  //   clearTimeout(that.ongoingGoToPoint[id]);
  //   clearTimeout(that.ongoingMoveMarker[id]);
  //   if (center) {
  //     map.setCenter(coords[0]);
  //   }

  //   function _goToPoint() {
  //     if (target > coords.length)
  //       return;

  //     var lat = mark.getPosition().lat;
  //     var lng = mark.getPosition().lng;
  //     var step = (speed * 1000 * delay) / 3600000; // in meters
  //     if (coords[target] == undefined)
  //       return;
  //     var dest = new LatLng(coords[target].lat, coords[target].lng);
  //     var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
  //     var numStep = distance / step;
  //     var i = 0;
  //     var deltaLat = (coords[target].lat - lat) / numStep;
  //     var deltaLng = (coords[target].lng - lng) / numStep;

  //     function _moveMarker() {
  //       lat += deltaLat;
  //       lng += deltaLng;
  //       i += step;

  //       if (i < distance) {
  //         var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng))

  //         head = head < 0 ? (360 + head) : head;
  //         console.log("if headdddddddddddd: ", head)
  //         if ((head != 0) || (head == NaN)) {
  //           mark.setRotation(head);
  //         }
  //         map.setCameraTarget(new LatLng(lat, lng));
  //         // mark.setIcon(icons);
  //         mark.setPosition(new LatLng(lat, lng));
  //         that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
  //       }
  //       else {
  //         var head = Spherical.computeHeading(mark.getPosition(), dest);

  //         head = head < 0 ? (360 + head) : head;
  //         console.log("else headdddddddddddd: ", head)

  //         if ((head != 0) || (head == NaN)) {
  //           mark.setRotation(head);
  //         }
  //         map.setCameraTarget(dest);
  //         // mark.setIcon(icons);
  //         mark.setPosition(dest);
  //         target++;

  //         that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
  //       }
  //     }
  //     _moveMarker();
  //   }
  //   _goToPoint();
  // }

  zoomin() {
    let that = this;
    // that.ngOnDestroy();
    that.allData.map.moveCameraZoomIn();
  }
  zoomout() {
    let that = this;
    // that.ngOnDestroy();
    that.allData.map.moveCameraZoomOut();
  }
  temp(data) {

    let that = this;
    that.showShareBtn = true;

    that.liveDataShare = data;
    that.onClickShow = false;

    // if (that.allData.map != undefined) {
    //   that.allData.map.remove();
    // }

    for (var i = 0; i < that.socketChnl.length; i++)
      that._io.removeAllListeners(that.socketChnl[i]);
    // that.allData = {};
    that.socketChnl = [];
    that.socketSwitch = {};
    let style = [];
    if (localStorage.getItem('NightMode') != null) {
      if (localStorage.getItem('NightMode') === 'ON') {
        //Change Style to night between 7pm to 5am
        if (this.isNight()) {
          style = mapStyle
        }
      }
    }
    // let mapOptions = {
    //   backgroundColor: 'white',
    //   controls: {
    //     compass: false,
    //     zoom: false,
    //     myLocation: true,
    //     myLocationButton: false,
    //   },
    //   gestures: {
    //     'rotate': false,
    //     'tilt': true,
    //     'scroll': true
    //   },
    //   gestureHandling: 'cooperative',
    //   camera: {
    //     target: { lat: 20.5937, lng: 78.9629 },
    //     zoom: this.zoomLevel,
    //   },
    //   mapType: that.mapKey,
    //   styles: style
    // }
    if (data) {
      if (data.last_location) {
        if (that.plt.is('android')) {
          // let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
          that.allData.map.moveCamera({
            target: {
              lat: data.last_location['lat'], lng: data.last_location['long']
            },
            zoom: this.zoomLevel
          });
          // that.allData.map = map;
          // that.onButtonClick();
        } else {
          if (that.plt.is('ios')) {
            // let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
            that.allData.map.moveCamera({
              target: {
                lat: data.last_location['lat'], lng: data.last_location['long']
              },
              zoom: this.zoomLevel
            });
            // Environment.setBackgroundColor("black");
            // that.allData.map = map;
          }
        }

        that.weDidntGetPing(data);
      } else {
        if (data.last_lat !== undefined && data.last_lng !== undefined) {
          if (that.plt.is('android')) {
            // let map = GoogleMaps.create('map_canvas_single_device', mapOptions);

            that.allData.map.moveCamera({
              target: {
                lat: data.last_lat, lng: data.last_lng
              },
              zoom: this.zoomLevel
            });
            // that.allData.map = map;
          } else {
            if (that.plt.is('ios')) {
              // let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
              that.allData.map.moveCamera({
                target: {
                  lat: data.last_lat, lng: data.last_lng
                },
                zoom: this.zoomLevel
              });
              // Environment.setBackgroundColor("black");
              // that.allData.map = map;
            }
          }
          that.weDidntGetPing(data);
        }
      }
      //** Check if traffice mode is on or off **//
      if (localStorage.getItem('TrafficMode') !== null) {
        if (localStorage.getItem('TrafficMode') === 'ON') {
          this.mapHideTraffic = true;
          this.allData.map.setTrafficEnabled(true);
        }
      }
      //** Check if traffice mode is on or off **//
    }
  }

  showTrail(lat, lng, map) {
    let that = this;
    that.tempArray.push({ lat: lat, lng: lng });

    if (that.tempArray.length === 2) {
      map.addPolyline({
        points: that.tempArray,
        color: '#0260f7',
        width: 5,
        geodesic: true
      }).then((polyline: Polyline) => {
        that.allData.polylineID = polyline;
        // that.polylineID.push(polyline);
      });
    } else if (that.tempArray.length > 2) {
      that.tempArray.shift();
      map.addPolyline({
        points: that.tempArray,
        color: '#0260f7',
        width: 5,
        geodesic: true
      }).then((polyline: Polyline) => {
        that.allData.polylineID = polyline;
        // that.allData.polylineID.push(polyline);
      });
    }
  }

  onClickMainMenu(item) {
    this.menuActive = !this.menuActive;
  }

  onSelectMapOption(type) {
    let that = this;
    if (type == 'locateme') {
      that.allData.map.setCameraTarget(that.latlngCenter);
    } else {
      if (type == 'mapHideTraffic') {
        this.mapHideTraffic = !this.mapHideTraffic;
        if (this.mapHideTraffic) {
          that.allData.map.setTrafficEnabled(true);
        } else {
          that.allData.map.setTrafficEnabled(false);
        }
      } else {
        if (type == 'mapTrail') {
          this.mapTrail = !this.mapTrail;
          if (this.mapTrail) {
            if (this.eyeBtnColor == '#d80622') {
              this.eyeBtnColor = '#f4f4f4';
              this.eyeColor = '#d80622';
            }
          } else {
            if (this.eyeBtnColor == '#f4f4f4') {
              this.eyeBtnColor = '#d80622';
              this.eyeColor = '#fff';
            }
          }

        }
      }
    }
  }

  showGallery() {
    console.log("inside show gallery")
    this.navCtrl.push('GalleryPage');
  }
}

@Component({
  templateUrl: './poi.html',
  selector: 'page-device-settings'
})
export class PoiPage implements OnInit {
  poi_name: any;
  address: any;
  params: any;
  lat: any;
  lng: any;
  islogin: any;

  constructor(public apicall: ApiServiceProvider, public toastCtrl: ToastController, public navparam: NavParams, public viewCtrl: ViewController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("cheack data", this.islogin);
    this.params = this.navparam.get("param1");
    console.log("cheacking", this.params);
    this.lat = this.params.mark.getPosition().lat;
    this.lng = this.params.mark.getPosition().lng;
  }

  ngOnInit() {
    let that = this;
    that.address = undefined;
    debugger
    if (this.lat != undefined || this.lng != undefined) {
      let tempcord = {
        "lat": this.lat,
        "long": this.lng
      }
      this.apicall.getAddress(tempcord)
        .subscribe(res => {
          if (res.message == "Address not found in databse") {
            var geocoder = new google.maps.Geocoder;
            var latlng = new google.maps.LatLng(this.lat, this.lng);
            var request = { "latLng": latlng };

            geocoder.geocode(request, function (resp, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                if (resp[0] != null) {
                  that.address = resp[0].formatted_address;
                } else {
                  console.log("No address available")
                }
              }
              else {
                that.address = 'N/A';
              }
            })
          } else {
            that.address = res.address;
          }
          // console.log(that.address);
        });
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  save() {
    if (this.poi_name == undefined || this.address == undefined) {
      let toast = this.toastCtrl.create({
        message: "POI name is required!",
        duration: 1500,
        position: "bottom"
      })
      toast.present();

    } else {
      var payload = {
        "poi": [{
          "location": {
            "type": "Point",
            "coordinates": [
              this.lng,
              this.lat
            ]
          },
          "poiname": this.poi_name,
          "status": "Active",
          "address": this.address,
          "user": this.islogin._id
        }]
      };
      this.apicall.startLoading().present();
      this.apicall.addPOIAPI(payload)
        .subscribe(data => {
          this.apicall.stopLoading();
          let toast = this.toastCtrl.create({
            message: "POI added successfully!",
            duration: 2000,
            position: 'top'
          });
          toast.present();
          this.viewCtrl.dismiss();
        },
          err => {
            this.apicall.stopLoading();
          })
    }
  }
}


