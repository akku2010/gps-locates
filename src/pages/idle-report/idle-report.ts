import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-idle-report',
  templateUrl: 'idle-report.html',
})
export class IdleReportPage implements OnInit {
  islogin: any;
  devices: any;
  portstemp: any;
  datetimeStart: string;
  datetimeEnd: string;
  device_id: any = [];
  minTime: number = 5;
  pData: any = [];
  selectedVehicle: any;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter IdleReportPage');
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {

    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getReport() {
    if (this.device_id.length === 0) {
      this.toastCtrl.create({
        message: 'Please select vehicle and try again..',
        duration: 1500,
        position: 'bottom'
      }).present();
      return;
    }
    var _bUrl = this.apiCall.mainUrl + 'stoppage/idleReportdatatable';
    var payload = {
      "draw": 3,
      "columns": [
        {
          "data": "_id"
        },
        {
          "data": "device"
        },
        {
          "data": "device.Device_Name"
        },
        {
          "data": "start_time"
        },
        {
          "data": "end_time"
        },
        {
          "data": "lat"
        },
        {
          "data": "long"
        },
        {
          "data": "ac_status"
        },
        {
          "data": "idle_time"
        },
        {
          "data": "address"
        },
        {
          "data": null,
          "defaultContent": ""
        }
      ],
      "order": [
        {
          "column": 0,
          "dir": "asc"
        }
      ],
      "start": 0,
      "length": 10,
      "search": {
        "value": "",
        "regex": false
      },
      "op": {},
      "select": [],
      "find": {
        "device": {
          "$in":
            this.device_id

        },
        "start_time": {
          "$gte": new Date(this.datetimeStart).toISOString()
        },
        "end_time": {
          "$lte": new Date(this.datetimeEnd).toISOString()
        },
        "idle_time": {
          "$gte": (this.minTime * 60000)
        }
      }
    };
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(_bUrl, payload)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log('idle report data: ', data)
        if (data.data.length > 0) {
          this.innerFunc(data.data);
        }
      });
  }
  innerFunc(pdata) {
    let outerthis = this;
    var i = 0, howManyTimes = pdata.length;

    function f() {
      // console.log("conversion: ", Number(outerthis.summaryReport[i].devObj[0].Mileage))
      // var hourconversion = 2.7777778 / 10000000;
      outerthis.pData.push(
        {
          'Device_Name': pdata[i].device.Device_Name,
          'start_location': {
            'lat': pdata[i].lat,
            'lng': pdata[i].long
          },
          'ac_status': (pdata[i].ac_status ? pdata[i].ac_status : 'NA'),
          'end_time': pdata[i].end_time,
          'duration': outerthis.parseMillisecondsIntoReadableTime(pdata[i].idle_time),
          'start_time': pdata[i].start_time
        });

      outerthis.start_address(outerthis.pData[i], i);


      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  start_address(item, index) {
    let that = this;
    //that.pData[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.pData[index].StartLocation = "N/A";
    } else if (item.start_location) {
      // debugger
      var payload = {
        "lat": item.start_location.lat,
        "long": item.start_location.lng,
        //"api_id": "1"
      }
      this.apiCall.getAddress(payload)
        .subscribe((res) => {
          if (res.message == "Address not found in databse") {
            this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.lng)
              .then(res => {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.start_location.lat, item.start_location.lng);
                that.pData[index].StartLocation = str;
                console.log("inside", that.pData[index].StartLocation);
              })
          } else {
            that.pData[index].StartLocation = res.address;
          }
        })
    }
  }

  getDeviceDetail(selectedVehicle) {
    // console.log(devData);
    // this.device_id = devData;
    this.device_id = [];
    if (selectedVehicle.length > 0) {
      if (selectedVehicle.length > 1) {
        for (var t = 0; t < selectedVehicle.length; t++) {
          this.device_id.push(selectedVehicle[t]._id)
        }
      } else {
        this.device_id.push(selectedVehicle[0]._id)
      }
    } else return;
  }

  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    // return h + ':' + m;
    return h + ':' + m + ':' + s;
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apiCall.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

}
