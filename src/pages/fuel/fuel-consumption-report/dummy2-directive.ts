import { Directive, Output, EventEmitter } from "@angular/core";

@Directive({
    selector: '[onCreate2]'
})

export class OnCreate2 {
    @Output() onCreate2: EventEmitter<any> = new EventEmitter<any>();
    constructor() {}
    ngOnInit() {
       this.onCreate2.emit('dummy');
    }
}
